import os
import unittest
from tempfile import TemporaryDirectory

import git

from mdml_tools.utils.hydra_callbacks import LogGitHash


class TestGitHashLogger(unittest.TestCase):
    def setUp(self) -> None:
        self.repo_destination = os.getcwd()
        repo = git.Repo(path=self.repo_destination, search_parent_directories=True)
        sha = repo.head.object.hexsha
        self.git_hash = sha

        self.tmp_dir = TemporaryDirectory()

    def test_logger_functionality(self):
        git_hash_tmp = LogGitHash.get_git_hash(self.repo_destination)
        self.assertEqual(self.git_hash, git_hash_tmp)

        LogGitHash.save_hash_to_file(self.git_hash, file_name=os.path.join(self.tmp_dir.name, "git_hash.txt"))
        git_hash_path = os.path.join(self.tmp_dir.name, "git_hash.txt")
        with open(git_hash_path) as file:
            git_hash_from_file = file.readline()
        self.assertEqual(self.git_hash, git_hash_from_file)

    def tearDown(self):
        self.tmp_dir.cleanup()
