import unittest

import hydra
import torch
from hydra.core.config_store import ConfigStore
from torch import nn

from mdml_tools.utils import add_hydra_models_to_config_store
from mdml_tools.utils.profiling import measure_execution_time, measure_execution_time_context


def _mock_log_func(arg1, arg2, arg3):
    if torch.cuda.is_available():
        if arg1 not in [
            "profile_time/_train_loop_method (ms)",
            "profile_memory_diff/_train_loop (mb)",
            "profile_time/_train_loop (ms)",
            "profile_memory_diff/_train_loop_method (mb)",
        ]:
            raise ValueError(f"Wrong function name: {arg1}")
        if not isinstance(arg2, float):
            raise ValueError("Wrong time format!")
        if not isinstance(arg3, int):
            raise ValueError("Wrong log_step format!")


class TestProfilingDecorator(unittest.TestCase):
    def setUp(self) -> None:
        torch.manual_seed(4)
        cs = ConfigStore.instance()
        cs = add_hydra_models_to_config_store(cs)
        self.cs = cs
        self.test_var = "Test"
        self.global_step = 0

    def _mock_log_method(self, arg1, arg2, arg3):
        assert isinstance(self.test_var, str)
        if torch.cuda.is_available():
            if arg1 not in [
                "profile_time/_train_loop_method (ms)",
                "profile_memory_diff/_train_loop (mb)",
                "profile_time/_train_loop (ms)",
                "profile_memory_diff/_train_loop_method (mb)",
            ]:
                raise ValueError(f"Wrong function name: {arg1}")
            if not isinstance(arg2, float):
                raise ValueError("Wrong time format!")

    @measure_execution_time(experiment_log_func=_mock_log_func)
    def _train_loop(self, _model, _optimizer, _loss, _data):
        for batch_data in _data:
            _optimizer.zero_grad()
            output = _model(batch_data)
            _loss_value = _loss(output, batch_data)
            _loss_value.backward()
            _optimizer.step()
            self.global_step += 1

        return _loss_value

    def test_training_with_function(self):
        self.global_step = 0
        activation = nn.ReLU()
        model = hydra.utils.instantiate(
            self.cs.repo["model"]["fully_connected_model_base.yaml"].node,
            activation=activation,
            num_classes=1,
            input_features=1,
            hidden_layers=[2],
        )
        self.assertIsNotNone(model)
        optimizer = hydra.utils.instantiate(
            self.cs.repo["optimizer"]["adam_base.yaml"].node,
            params=model.parameters(),
        )
        self.assertIsNotNone(optimizer)
        loss = nn.MSELoss()
        data = torch.full(size=(10000, 64, 1), fill_value=1.0)

        loss_value = self._train_loop(model, optimizer, loss, data)

        self.assertAlmostEqual(loss_value.item(), 0.0, places=2)

    @measure_execution_time(experiment_log_func="self._mock_log_method", is_class_method=True)
    def _train_loop_method(self, _model, _optimizer, _loss, _data):
        for batch_data in _data:
            _optimizer.zero_grad()
            output = _model(batch_data)
            _loss_value = _loss(output, batch_data)
            _loss_value.backward()
            _optimizer.step()
            self.global_step += 1

        return _loss_value

    def test_training_with_method(self):
        self.global_step = 0
        activation = nn.ReLU()
        model = hydra.utils.instantiate(
            self.cs.repo["model"]["fully_connected_model_base.yaml"].node,
            activation=activation,
            num_classes=1,
            input_features=1,
            hidden_layers=[2],
        )
        self.assertIsNotNone(model)
        optimizer = hydra.utils.instantiate(
            self.cs.repo["optimizer"]["adam_base.yaml"].node,
            params=model.parameters(),
        )
        self.assertIsNotNone(optimizer)
        loss = nn.MSELoss()
        data = torch.full(size=(10000, 64, 1), fill_value=1.0)

        loss_value = self._train_loop_method(model, optimizer, loss, data)

        self.assertAlmostEqual(loss_value.item(), 0.0, places=2)


class TestProfilingContext(unittest.TestCase):
    def setUp(self) -> None:
        torch.manual_seed(4)
        cs = ConfigStore.instance()
        cs = add_hydra_models_to_config_store(cs)
        self.cs = cs
        self.test_var = "Test"
        self.global_step = 0

    def _mock_log_method(self, arg1, arg2, arg3):
        assert isinstance(self.test_var, str)
        if torch.cuda.is_available():
            if arg1 not in [
                "profile_time/train_loop_context (ms)",
                "profile_memory_diff/train_loop_context (mb)",
            ]:
                raise ValueError(f"Wrong function name: {arg1}")
            if not isinstance(arg2, float):
                raise ValueError("Wrong time format!")

    def _train_loop(self, _model, _optimizer, _loss, _data):
        with measure_execution_time_context(
            context_name="train_loop_context", experiment_log_func=self._mock_log_method
        ):
            for batch_data in _data:
                _optimizer.zero_grad()
                output = _model(batch_data)
                _loss_value = _loss(output, batch_data)
                _loss_value.backward()
                _optimizer.step()
                self.global_step += 1

        return _loss_value

    def test_training_with_context(self):
        self.global_step = 0
        activation = nn.ReLU()
        model = hydra.utils.instantiate(
            self.cs.repo["model"]["fully_connected_model_base.yaml"].node,
            activation=activation,
            num_classes=1,
            input_features=1,
            hidden_layers=[2],
        )
        self.assertIsNotNone(model)
        optimizer = hydra.utils.instantiate(
            self.cs.repo["optimizer"]["adam_base.yaml"].node,
            params=model.parameters(),
        )
        self.assertIsNotNone(optimizer)
        loss = nn.MSELoss()
        data = torch.full(size=(10000, 64, 1), fill_value=1.0)

        loss_value = self._train_loop(model, optimizer, loss, data)

        self.assertAlmostEqual(loss_value.item(), 0.0, places=2)
