import contextlib
import io
import logging
import os
import shutil
import time
import unittest

import torch
from omegaconf import OmegaConf

import mdml_tools.utils.logging as mdml_logging


class TestLogging(unittest.TestCase):
    def setUp(self) -> None:
        mdml_logging.set_log_levels("DEBUG")
        torch.manual_seed(42)

    def test_get_logger(self):
        logger = mdml_logging.get_logger("test")
        f = io.StringIO()
        stream_handler = mdml_logging.StreamHandler(f)
        logger.addHandler(stream_handler)

        with contextlib.redirect_stderr(f):
            logger.info("test")

        self.assertEqual("test", f.getvalue().strip())

    def test_set_log_level(self):
        levels = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]

        for level in levels:
            # convert level to int
            int_level = logging.getLevelName(level)

            mdml_logging.set_log_levels(level)
            logger = mdml_logging.get_logger(f"test_{level}")
            self.assertEqual(int_level, logger.getEffectiveLevel())

    def test_get_hparams(self):
        # create a dictconfig
        config = OmegaConf.create(
            {
                "model": {
                    "name": "test",
                    "params": {
                        "a": 1,
                        "b": 2,
                    },
                },
                "data": {
                    "name": "test",
                    "params": {
                        "a": 1,
                        "b": 2,
                    },
                },
            }
        )

        # create a model
        model = torch.nn.Linear(10, 10)

        # get hparams
        hparams = mdml_logging.get_hparams_from_hydra_config(config, model)

        # check that the hparams are correct
        self.assertEqual(hparams["name"], "test")
        self.assertEqual(hparams["params"]["a"], 1)
        self.assertEqual(hparams["params"]["b"], 2)
        self.assertEqual(hparams["params_total"], 110)
        self.assertEqual(hparams["params_trainable"], 110)
        self.assertEqual(hparams["params_not_trainable"], 0)

    def test_dump_all_locals(self):
        def _func(_logger):
            a = 1  # noqa: F841
            b = 2  # noqa: F841
            _c = 3  # noqa: F841
            mdml_logging.dump_all_locals(_logger)

        mdml_logging.set_log_levels("DEBUG")
        logger = mdml_logging.get_logger("test_dump_all_locals")
        f = io.StringIO()
        stream_handler = mdml_logging.StreamHandler(f)
        logger.addHandler(stream_handler)

        with contextlib.redirect_stderr(f):
            _func(_logger=logger)

        logs = f.getvalue()
        self.assertIn("_logger = <Logger test_dump_all_locals (DEBUG)>", logs)
        self.assertIn("a = 1", logs)
        self.assertIn("b = 2", logs)
        self.assertIn("_c = 3", logs)


class TestTBLogger(unittest.TestCase):
    def setUp(self) -> None:
        torch.manual_seed(42)
        mdml_logging.set_log_levels("DEBUG")

    def tearDown(self) -> None:
        if os.path.exists("tb_logs"):
            time.sleep(1)  # wait for tensorboard to finish writing
            shutil.rmtree("tb_logs")

        mdml_logging.TB_LOGGER = None

    def test_set_tb_logger(self):
        logger = mdml_logging.get_logger("test_tb")
        tb_logger = mdml_logging.TensorBoardLogger("tb_logs")
        mdml_logging.set_tb_logger(tb_logger)
        f = io.StringIO()
        stream_handler = mdml_logging.StreamHandler(f)
        logger.addHandler(stream_handler)

        with contextlib.redirect_stderr(f):
            logger.error("test")

        time.sleep(1)  # wait for tensorboard to finish writing
        self.assertEqual("test", f.getvalue().strip())
        self.assertEqual(mdml_logging.TB_LOGGER, tb_logger)

        # test that the logger is actually used
        tb_logger.log_metrics({"test": 1})
        logdir = tb_logger.experiment.get_logdir()
        self.assertTrue(os.path.isdir(logdir))

        del tb_logger
