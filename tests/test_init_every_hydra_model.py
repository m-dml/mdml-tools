import unittest
from tempfile import TemporaryDirectory

import hydra
import torch
from hydra.core.config_store import ConfigStore
from hydra.errors import InstantiationException

from mdml_tools.hydra_models import (
    LightningCallback,
    LightningLogger,
    LightningProfiler,
    LightningStrategy,
    Loss,
    Metric,
    Model,
    Optimizer,
    Scheduler,
    Trainer,
)
from mdml_tools.utils import add_hydra_models_to_config_store


class TestHydraInits(unittest.TestCase):
    def setUp(self) -> None:
        self.cs = ConfigStore.instance()
        self.cs = add_hydra_models_to_config_store(self.cs)
        self.repo = self.cs.repo
        self.temp_dir = TemporaryDirectory()

    def test_optimizer_init(self):
        elements = self.repo["optimizer"]
        model = torch.nn.Linear(1, 1)
        optimizer = None
        for element in elements.values():
            if element.node._target_ == "torch.optim.optimizer.Optimizer":
                continue
            try:
                optimizer: Optimizer = hydra.utils.instantiate(element.node, params=model.parameters())
                self.assertIsInstance(optimizer, torch.optim.Optimizer)
            except InstantiationException as exc:
                if "not supported" in str(exc):
                    continue
                else:
                    raise exc
        self.assertIsNotNone(optimizer)

    def test_loss_init(self):
        elements = self.repo["loss"]
        for element in elements.values():
            loss: Loss = hydra.utils.instantiate(element.node)
            self.assertIsInstance(loss, torch.nn.modules.loss._Loss)

    def test_scheduler_init(self):
        elements = self.repo["scheduler"]
        optimizer = torch.optim.SGD(torch.nn.Linear(1, 1).parameters(), lr=1e-3)
        for element in elements.values():
            try:
                scheduler: Scheduler = hydra.utils.instantiate(element.node, optimizer=optimizer)
                self.assertIsNotNone(scheduler)
            except InstantiationException as exc:
                exceptions_allowed = [
                    "not supported",
                    "unsupported operand",
                    "must define either",
                    "'NoneType' object is not callable",
                    "__init__() got an unexpected keyword argument 'optimizer'",
                    "object of type 'NoneType' has no",
                ]
                if any([exception in str(exc) for exception in exceptions_allowed]):
                    continue
                else:
                    raise exc

    def test_lightning_callback_init(self):
        from pytorch_lightning.callbacks import Callback

        elements = self.repo["lightning_callback"]
        for element in elements.values():
            for sub_element in element.values():
                try:
                    callback: LightningCallback = hydra.utils.instantiate(sub_element.node)
                    self.assertIsNotNone(callback)
                    self.assertIsInstance(callback, Callback)
                except InstantiationException as exc:
                    skips = ["TypeError", "MisconfigurationException"]
                    if any(s in str(exc) for s in skips):
                        continue
                    else:
                        raise exc

    def test_lightning_logger_init(self):
        from pytorch_lightning.loggers.logger import Logger

        elements = self.repo["lightning_logger"]
        for element in elements.values():
            for sub_element in element.values():
                if "save_dir" in sub_element.node.keys():  # only test loggers that can save data (not base logger)
                    sub_element.node["save_dir"] = self.temp_dir.name
                    logger: LightningLogger = hydra.utils.instantiate(sub_element.node)
                    self.assertIsNotNone(logger)
                    self.assertIsInstance(logger, Logger)

    def test_metrics_init(self):
        only_test_selected = ["Accuracy", "AUC", "F1", "Precision", "Recall", "RocAuc"]
        elements = self.repo["metric"]
        metric = None
        for element in elements.values():
            for sub_element in element.values():
                if not any(s in sub_element.node._target_ for s in only_test_selected):
                    continue
                metric: Metric = hydra.utils.instantiate(sub_element.node, task="binary")
                self.assertIsNotNone(metric)
        self.assertIsNotNone(metric)

    def test_strategies_init(self):
        elements = self.repo["lightning_strategy"]
        for element in elements.values():
            run_only = ["DDPStrategy", "SingleDeviceStrategy"]
            if not any(s in element.node._target_ for s in run_only):
                continue
            strategy: LightningStrategy = hydra.utils.instantiate(element.node)
            self.assertIsNotNone(strategy)

    def test_profiler_init(self):
        elements = self.repo["lightning_profiler"]
        for element in elements.values():
            skip = ["XLAProfiler", "pytorch_lightning.profilers.profiler.Profiler"]
            if any(s in element.node._target_ for s in skip):
                continue

            profiler: LightningProfiler = hydra.utils.instantiate(element.node)
            self.assertIsNotNone(profiler)

    def test_trainer_init(self):
        elements = self.repo["lightning_trainer"]
        for element in elements.values():
            trainer: Trainer = hydra.utils.instantiate(element.node)
            self.assertIsNotNone(trainer)

    def test_models_init(self):
        elements = self.repo["model"]
        for element in elements.values():
            kwargs = {}
            if "FullyConnectedModel" in element.node._target_:
                kwargs = dict(activation=torch.nn.ReLU(), num_classes=2, normalize=True)

            if "CustomResnet" in element.node._target_:
                resnet = torch.hub.load("pytorch/vision:v0.6.0", "resnet18", pretrained=False)
                kwargs = dict(model=resnet, maxpool1=False)

            model: Model = hydra.utils.instantiate(element.node, **kwargs)
            self.assertIsNotNone(model)
            self.assertIsInstance(model, torch.nn.Module)

    def test_dataloader_init(self):
        from .helpers import DummyDataset

        dataloader = self.repo["dataloading"]["data_loader_base.yaml"]
        if "num_workers" in dataloader.node.keys():
            dataloader.node.num_workers = 1
        dataloader = hydra.utils.instantiate(dataloader.node, dataset=DummyDataset())
        self.assertIsNotNone(dataloader)

    def test_preprocessing_init(self):
        elements = self.repo["preprocessing"]
        for element in elements.values():
            if element.name.startswith("_"):
                continue
            with self.assertRaises(
                InstantiationException,
                msg="data_loader_samples is relative, but data_loader does not have a __len__ method. Please provide "
                "an absolute number of samples.",
            ):
                _ = hydra.utils.instantiate(element.node)

    def test_activations_init(self):
        elements = self.repo["activation"]
        for element in elements.values():
            try:
                activation = hydra.utils.instantiate(element.node)
                self.assertIsNotNone(activation)
            except Exception as exc:
                if "unsupported operand" in str(exc):
                    continue
                elif "not supported" in str(exc):
                    continue
                else:
                    raise exc

    def test_simulators_init(self):
        elements = self.repo["simulator"]
        for element in elements.values():
            simulator = hydra.utils.instantiate(element.node)
            self.assertIsNotNone(simulator)

    def test_convolutions_init(self):
        elements = self.repo["convolution"]
        for element in elements.values():
            try:
                convolution = hydra.utils.instantiate(element.node)
                self.assertIsNotNone(convolution)
            except InstantiationException as exc:
                skips = ["TypeError"]
                if any(s in str(exc) for s in skips):
                    continue
                else:
                    raise exc

    def test_pooling_init(self):
        elements = self.repo["pooling"]
        for element in elements.values():
            try:
                pooling = hydra.utils.instantiate(element.node)
                self.assertIsNotNone(pooling)
            except InstantiationException as exc:
                exceptions_allowed = [
                    "requires specifying either an output size, or a pooling ratio",
                ]
                if any([exception in str(exc) for exception in exceptions_allowed]):
                    continue
                else:
                    raise exc

    def test_batch_norm_init(self):
        elements = self.repo["batch_norm"]
        for element in elements.values():
            try:
                batch_norm = hydra.utils.instantiate(element.node)
                self.assertIsNotNone(batch_norm)
            except InstantiationException as exc:
                skips = ["TypeError"]
                if any(s in str(exc) for s in skips):
                    continue
                else:
                    raise exc

    def test_dropout_init(self):
        elements = self.repo["dropout"]
        for element in elements.values():
            dropout = hydra.utils.instantiate(element.node)
            self.assertIsNotNone(dropout)

    def tearDown(self) -> None:
        self.temp_dir.cleanup()
