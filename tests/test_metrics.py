import unittest

import torch

from mdml_tools.utils.metrics import RootEuclidianDistance


class TestRootEuclidianDistance(unittest.TestCase):
    def test_1_init(self):
        metric = RootEuclidianDistance()
        self.assertIsNotNone(metric)

    def test_2_forward_identity(self):
        metric = RootEuclidianDistance()
        inputs = torch.tensor([[1.0, 2.0]])
        targets = torch.tensor([[1.0, 2.0]])
        loss = metric(inputs, targets)
        self.assertAlmostEqual(loss.item(), 0.0, places=2)

    def test_3_forward_negative_number(self):
        metric = RootEuclidianDistance()
        inputs = torch.tensor([[1.0, 2.0]])
        targets = torch.tensor([[1.0, -2.0]])
        loss = metric(inputs, targets)
        self.assertGreater(loss.item(), 0.0)

    def test_4_forward_root_3(self):
        metric = RootEuclidianDistance(root=3)
        inputs = torch.tensor([[1.0, 2.0]])
        targets = torch.tensor([[1.0, 2.0]])
        loss = metric(inputs, targets)
        self.assertAlmostEqual(loss.item(), 0.0, places=2)

    def test_5_forward_root_3_negative_number(self):
        metric = RootEuclidianDistance(root=3)
        inputs = torch.tensor([[1.0, 2.0]])
        targets = torch.tensor([[1.0, -2.0]])
        loss = metric(inputs, targets)
        self.assertGreater(loss.item(), 0.0)

    def test_6_forward_4D_inputs(self):
        metric = RootEuclidianDistance()
        inputs = torch.tensor([[[[1.0, 2.0]]]])
        targets = torch.tensor([[[[1.0, 2.0]]]])
        loss = metric(inputs, targets)
        self.assertAlmostEqual(loss.item(), 0.0, places=2)

    def test_7_forward_4D_inputs_no_reduction(self):
        metric = RootEuclidianDistance()
        inputs = torch.ones(size=(1000, 1, 2, 3))
        targets = torch.zeros(size=(1000, 1, 2, 3))
        loss = metric(inputs, targets, reduction="none")
        self.assertAlmostEqual(loss.mean().item(), 1.0, places=2)
        self.assertEqual(loss.shape, torch.Size([1000]))

    def test_8_compare_to_mse(self):
        """When root=0.5 the metric should be equal to the MSE."""
        metric = RootEuclidianDistance(root=0.5)
        inputs = torch.rand(size=(1000, 1, 2, 3))
        targets = torch.rand(size=(1000, 1, 2, 3))
        loss = metric(inputs, targets)
        mse = torch.nn.MSELoss()(inputs, targets)
        self.assertAlmostEqual(loss.item(), mse.item(), places=8)
