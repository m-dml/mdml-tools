import glob
import importlib
import os
import unittest

from hydra.core.config_store import ConfigStore

from mdml_tools import hydra_models
from mdml_tools.utils.add_hydra_models_to_config_store import (
    _to_snake_case,
    add_hydra_models_to_config_store,
    CONFIG_GROUPS,
)


class TestAddedCategories(unittest.TestCase):
    def setUp(self) -> None:
        self.cs = ConfigStore.instance()
        self.cs = add_hydra_models_to_config_store(self.cs)

    def test_if_all_categories_are_added(self):
        self.assertCountEqual(
            self.cs.repo.keys(), list(CONFIG_GROUPS.keys()) + ["hydra"] + ["_dummy_empty_config_.yaml"]
        )

    def test_all_models_are_added(self):
        files = glob.glob(hydra_models.__path__[0] + "/*.py")

        # remove __init__.py and typing.py
        files = [f for f in files if "__init__" not in f and "typing" not in f]

        print(f"All files: {files}")

        for file in files:
            module = importlib.import_module("mdml_tools.hydra_models." + os.path.basename(file).split(".")[0])
            classes = [getattr(module, name) for name in dir(module) if isinstance(getattr(module, name), type)]

            group = os.path.basename(file).split(".")[0]
            n_added_models = len(self.cs.repo[group])

            n_expected_models = len(classes)

            added_models_names = sorted(
                [model_name.split(".yaml")[0].split("_base")[0] for model_name in self.cs.repo[group]]
            )
            expected_models_names = sorted([_to_snake_case(model.__name__) for model in classes])

            # find out which models are not in both lists:
            added_models_names_not_in_expected_models_names = [
                model_name for model_name in added_models_names if model_name not in expected_models_names
            ]

            expected_models_names_not_in_added_models_names = [
                model_name for model_name in expected_models_names if model_name not in added_models_names
            ]

            if "enum" in expected_models_names_not_in_added_models_names:
                expected_models_names_not_in_added_models_names.remove("enum")
                n_expected_models -= 1

            self.assertEqual(
                n_added_models,
                n_expected_models,
                f"Number of models in group {group} does not match. Expected {n_expected_models}, "
                f"got {n_added_models}. \n"
                f"Models that were added but should not have been: {added_models_names_not_in_expected_models_names}\n"
                f"Models that were not added but should have been: {expected_models_names_not_in_added_models_names}\n",
            )


class TestRenameConfigStoreGroups(unittest.TestCase):
    def test_renaming(self):
        original_cs = ConfigStore.instance()
        renamed_cs = add_hydra_models_to_config_store(
            original_cs, rename_groups={"distribution": "distributions_renamed"}
        )
        self.assertIn("distributions_renamed", renamed_cs.repo.keys())


class TestRenameConfigStoreGroupsWithMultipleValuesPerKey(unittest.TestCase):
    def test_clone_group(self):
        original_cs = ConfigStore.instance()
        modified_cs = add_hydra_models_to_config_store(
            original_cs, rename_groups={"distribution": ["distributions_one", "distributions_two"]}
        )
        self.assertIn("distributions_one", modified_cs.repo.keys())
        self.assertIn("distributions_two", modified_cs.repo.keys())
        self.assertCountEqual(modified_cs.repo["distributions_one"], modified_cs.repo["distributions_two"])
