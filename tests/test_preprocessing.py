import unittest

import torch

from mdml_tools.utils.preprocessing import MinMaxScaler, NormalScaler


class MockDataLoader:
    def __init__(self, size=(10000, 32, 1)):
        torch.manual_seed(4)
        self._x_data = torch.randn(size=size)
        self._y_data = torch.empty(size=size)
        self._noise_data = torch.randn(size=size)

        self.x_data_min = self._x_data.min().item()
        self.x_data_max = self._x_data.max().item()
        self.x_data_mean = self._x_data.mean().item()
        self.x_data_std = self._x_data.std().item()

    def __iter__(self):
        for _x, _y, _noise in zip(self._x_data, self._y_data, self._noise_data):
            yield _x, _y, _noise

    def __len__(self):
        return len(self._x_data)


class TestMinmaxScaler(unittest.TestCase):
    def setUp(self) -> None:
        torch.manual_seed(4)
        self.data_loader = MockDataLoader()

    def test_fitting_on_data(self):
        scaler = MinMaxScaler(data_loader=self.data_loader, data_loader_dim=0, data_loader_samples=1)
        self.assertAlmostEqual(scaler.data_min, self.data_loader.x_data_min)
        self.assertAlmostEqual(scaler.data_max, self.data_loader.x_data_max)
        x_scaled = scaler.transform(self.data_loader._x_data)
        self.assertAlmostEqual(x_scaled.min().item(), 0)
        self.assertAlmostEqual(x_scaled.max().item(), 1)

        x_rescaled = scaler.inverse_transform(x_scaled)
        self.assertAlmostEqual(x_rescaled.min().item(), self.data_loader.x_data_min, places=5)
        self.assertAlmostEqual(x_rescaled.max().item(), self.data_loader.x_data_max, places=5)

    def test_fitting_on_data_with_clip(self):
        scaler = MinMaxScaler(clip_min=0, clip_max=1, data_min=0.4, data_max=0.5, data_loader_samples=1000)

        x_scaled = scaler.transform(self.data_loader._x_data)
        self.assertAlmostEqual(x_scaled.min().item(), 0, places=5)
        self.assertAlmostEqual(x_scaled.max().item(), 1, places=5)

        x_rescaled = scaler.inverse_transform(x_scaled)
        self.assertLess(0, x_rescaled.min().item())
        self.assertGreater(1, x_rescaled.max().item())

    def test_fitting_with_target_range(self):
        scaler = MinMaxScaler(target_min=2, target_max=5, data_loader=self.data_loader, data_loader_dim=0)

        self.assertAlmostEqual(scaler.data_min, self.data_loader.x_data_min)
        self.assertAlmostEqual(scaler.data_max, self.data_loader.x_data_max)
        x_scaled = scaler.transform(self.data_loader._x_data)
        self.assertAlmostEqual(x_scaled.min().item(), 2)
        self.assertAlmostEqual(x_scaled.max().item(), 5)

        x_rescaled = scaler.inverse_transform(x_scaled)
        self.assertAlmostEqual(x_rescaled.min().item(), self.data_loader.x_data_min, places=5)
        self.assertAlmostEqual(x_rescaled.max().item(), self.data_loader.x_data_max, places=5)

    def test_fitting_without_explicit_dataloader_dim(self):
        scaler = MinMaxScaler(data_loader=self.data_loader)

        self.assertAlmostEqual(scaler.data_min, self.data_loader.x_data_min)
        self.assertAlmostEqual(scaler.data_max, self.data_loader.x_data_max)
        x_scaled = scaler.transform(self.data_loader._x_data)
        self.assertAlmostEqual(x_scaled.min().item(), 0)
        self.assertAlmostEqual(x_scaled.max().item(), 1)

        x_rescaled = scaler.inverse_transform(x_scaled)
        self.assertAlmostEqual(x_rescaled.min().item(), self.data_loader.x_data_min, places=5)
        self.assertAlmostEqual(x_rescaled.max().item(), self.data_loader.x_data_max, places=5)

    def test_fitting_on_relative_number_of_samples(self):
        for samples in [0.1, 0.5, 1]:
            scaler = MinMaxScaler(
                data_loader=self.data_loader, data_loader_dim=0, data_loader_samples=samples, clip_min=0, clip_max=1
            )
            x_scaled = scaler.transform(self.data_loader._x_data)
            self.assertAlmostEqual(x_scaled.min().item(), 0)
            self.assertAlmostEqual(x_scaled.max().item(), 1)

            x_rescaled = scaler.inverse_transform(x_scaled)
            self.assertEqual(x_rescaled.shape, x_scaled.shape)


class TestNormalScaler(unittest.TestCase):
    def setUp(self) -> None:
        torch.manual_seed(4)
        self.data_loader = MockDataLoader()

    def test_fitting_on_data(self):
        scaler = NormalScaler(data_loader=self.data_loader, data_loader_dim=0, data_loader_samples=1)
        self.assertAlmostEqual(scaler.data_mean, self.data_loader._x_data.mean().item(), places=1)
        self.assertAlmostEqual(scaler.data_std, self.data_loader._x_data.std().item(), places=1)
        x_scaled = scaler.transform(self.data_loader._x_data)
        self.assertAlmostEqual(x_scaled.mean().item(), 0, places=1)
        self.assertAlmostEqual(x_scaled.std().item(), 1, places=1)

        x_rescaled = scaler.inverse_transform(x_scaled)
        self.assertAlmostEqual(x_rescaled.mean().item(), self.data_loader._x_data.mean().item(), places=5)
        self.assertAlmostEqual(x_rescaled.std().item(), self.data_loader._x_data.std().item(), places=5)

    def test_fitting_with_target_range(self):
        scaler = NormalScaler(
            target_mean=2, target_std=5, data_loader=self.data_loader, data_loader_dim=0, max_memory_usage=1e4
        )

        self.assertAlmostEqual(scaler.data_mean, self.data_loader._x_data.mean().item(), places=1)
        self.assertAlmostEqual(scaler.data_std, self.data_loader._x_data.std().item(), places=1)
        x_scaled = scaler.transform(self.data_loader._x_data)
        self.assertAlmostEqual(x_scaled.mean().item(), 2, places=1)
        self.assertAlmostEqual(x_scaled.std().item(), 5, places=1)

        x_rescaled = scaler.inverse_transform(x_scaled)
        self.assertAlmostEqual(x_rescaled.mean().item(), self.data_loader._x_data.mean().item(), places=5)
        self.assertAlmostEqual(x_rescaled.std().item(), self.data_loader._x_data.std().item(), places=5)

    def test_fitting_without_explicit_dataloader_dim(self):
        scaler = NormalScaler(data_loader=self.data_loader)

        self.assertAlmostEqual(scaler.data_mean, self.data_loader._x_data.mean().item(), places=1)
        self.assertAlmostEqual(scaler.data_std, self.data_loader._x_data.std().item(), places=1)
        x_scaled = scaler.transform(self.data_loader._x_data)
        self.assertAlmostEqual(x_scaled.mean().item(), 0, places=1)
        self.assertAlmostEqual(x_scaled.std().item(), 1, places=1)

        x_rescaled = scaler.inverse_transform(x_scaled)
        self.assertAlmostEqual(x_rescaled.mean().item(), self.data_loader._x_data.mean().item(), places=5)
        self.assertAlmostEqual(x_rescaled.std().item(), self.data_loader._x_data.std().item(), places=5)

    def test_fitting_on_part_of_dataloader(self):
        for samples in [0.1, 0.5, 1]:
            scaler = NormalScaler(data_loader=self.data_loader, data_loader_dim=0, data_loader_samples=samples)
            x_scaled = scaler.transform(self.data_loader._x_data)
            self.assertAlmostEqual(x_scaled.mean().item(), 0, places=1)
            self.assertAlmostEqual(x_scaled.std().item(), 1, places=1)

            x_rescaled = scaler.inverse_transform(x_scaled)
            self.assertEqual(x_rescaled.shape, x_scaled.shape)

    def test_fitting_on_gamma_distribution(self):
        data_loader = MockDataLoader()
        data_loader._x_data = torch.distributions.gamma.Gamma(1, 1).sample(sample_shape=[10000, 32, 1])
        scaler = NormalScaler(data_loader=data_loader, data_loader_dim=0, data_loader_samples=1)
        self.assertAlmostEqual(scaler.data_mean, data_loader._x_data.mean().item(), places=1)
        self.assertAlmostEqual(scaler.data_std, data_loader._x_data.std().item(), places=1)
        x_scaled = scaler.transform(data_loader._x_data)
        self.assertAlmostEqual(x_scaled.mean().item(), 0, places=1)
        self.assertAlmostEqual(x_scaled.std().item(), 1, places=1)

        x_rescaled = scaler.inverse_transform(x_scaled)
        self.assertTrue(torch.allclose(x_rescaled, data_loader._x_data, atol=1e-4))
        self.assertAlmostEqual(x_rescaled.mean().item(), data_loader._x_data.mean().item(), places=5)
        self.assertAlmostEqual(x_rescaled.std().item(), data_loader._x_data.std().item(), places=5)

    def test_on_2D_data(self):
        data_loader = MockDataLoader()
        data_loader._x_data = torch.randn(1000, 4, 24, 24)
        scaler = NormalScaler(data_loader=data_loader, data_loader_dim=0, data_loader_samples=1)
        x_scaled = scaler.transform(data_loader._x_data)
        self.assertAlmostEqual(x_scaled.mean().item(), 0, places=1)
        self.assertAlmostEqual(x_scaled.std().item(), 1, places=1)

        x_rescaled = scaler.inverse_transform(x_scaled)
        self.assertTrue(torch.allclose(x_rescaled, data_loader._x_data, atol=1e-4))
        self.assertAlmostEqual(x_rescaled.mean().item(), data_loader._x_data.mean().item(), places=5)
        self.assertAlmostEqual(x_rescaled.std().item(), data_loader._x_data.std().item(), places=5)

    def test_on_3D_data(self):
        data_loader = MockDataLoader()
        data_loader._x_data = torch.randn(1000, 4, 12, 12, 2)
        scaler = NormalScaler(data_loader=data_loader, data_loader_dim=0, data_loader_samples=1)
        x_scaled = scaler.transform(data_loader._x_data)
        self.assertAlmostEqual(x_scaled.mean().item(), 0, places=1)
        self.assertAlmostEqual(x_scaled.std().item(), 1, places=1)

        x_rescaled = scaler.inverse_transform(x_scaled)
        self.assertTrue(torch.allclose(x_rescaled, data_loader._x_data, atol=1e-4))
        self.assertAlmostEqual(x_rescaled.mean().item(), data_loader._x_data.mean().item(), places=5)
        self.assertAlmostEqual(x_rescaled.std().item(), data_loader._x_data.std().item(), places=5)

    def perform_iterative_test(self, scaler):
        normal_data = torch.randn(100000, 32, 24)
        self.assertAlmostEqual(normal_data.mean().item(), 0, places=3)
        self.assertAlmostEqual(normal_data.std().item(), 1, places=3)

        x_scaled = normal_data

        for n in range(5):
            x_scaled = scaler.transform(x_scaled)
            self.assertAlmostEqual(
                x_scaled.mean().item(),
                0,
                places=3,
                msg=f"After {n}th transformation. Original mean: {normal_data.mean().item()}",
            )
            self.assertAlmostEqual(
                x_scaled.std().item(),
                1,
                places=3,
                msg=f"After {n}th transformation. Original std: {normal_data.std().item()}",
            )

    def test_transforming_normal_multiple_times_stays_normal_after_fitting_on_data_loader(self):
        data_loader = MockDataLoader(size=[100000, 32, 24])
        scaler = NormalScaler(data_loader=data_loader, data_loader_dim=0)
        scaler.fit()

        # first check that the data actually has mean 0 and std 1
        self.assertAlmostEqual(data_loader._x_data.mean().item(), 0, places=3)
        self.assertAlmostEqual(data_loader._x_data.std().item(), 1, places=3)

        # then check that the scaler has inferred the correct mean and std
        self.assertAlmostEqual(scaler.data_mean, 0, places=3)
        self.assertAlmostEqual(scaler.data_std, 1, places=3)

        self.perform_iterative_test(scaler)

    def test_transforming_normal_multiple_times_stays_normal_after_giving_explicit_mean_and_std(self):
        scaler = NormalScaler(data_mean=0, data_std=1)
        self.perform_iterative_test(scaler)

    def test_transforming_normal_multiple_times_stays_normal_after_fitting_on_data_loader_with_restricted_memory(self):
        data_loader = MockDataLoader(size=[100000, 32, 24])
        data = data_loader._x_data

        # check that the data is larger than our set memory limit
        data_size = data.element_size() * data.nelement()
        # print(f"Data size: {data_size}")
        self.assertGreater(data_size, 1e6)

        # fit the scaler
        scaler = NormalScaler(data_loader=data_loader, data_loader_dim=0, max_memory_usage=1e6)
        scaler.fit()

        # first check that the data actually has mean 0 and std 1
        self.assertAlmostEqual(data_loader._x_data.mean().item(), 0, places=3)
        self.assertAlmostEqual(data_loader._x_data.std().item(), 1, places=3)

        # then check that the scaler has inferred the correct mean and std
        self.assertAlmostEqual(scaler.data_mean, 0, places=3)
        self.assertAlmostEqual(scaler.data_std, 1, places=3)

        self.perform_iterative_test(scaler)
