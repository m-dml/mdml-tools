import torch
from torch.utils.data import Dataset


class DummyDataset(Dataset):
    def __init__(self, size=100):
        super().__init__()
        self.size = size

    def __getitem__(self, item):
        return torch.rand(1), torch.rand(1)

    def __len__(self):
        return self.size
