import inspect
from typing import Any, Callable, Optional, Union
from unittest import TestCase

import torch
from torch.utils.data import Dataset

from mdml_tools.scripts.convert2hydra import get_types_from_signature


class TestGetTypesFromSignature(TestCase):
    def test_without_type(self):
        def test_function(a, b):
            pass

        for key, value in inspect.signature(test_function).parameters.items():
            type_str = get_types_from_signature(value)
            self.assertEqual("Any", type_str)

    def test_simple_types_without_defaults(self):
        def test_function(
            int_: int,
            float_: float,
            str_: str,
            bool_: bool,
            list_: list,
            tuple_: tuple,
            dict_: dict,
            set_: set,
            Any_: Any,
        ):
            pass

        for key, value in inspect.signature(test_function).parameters.items():
            type_str = get_types_from_signature(value)
            self.assertEqual(key.split("_")[0], type_str)

    def test_simple_types_with_defaults(self):
        def test_function(
            int_: int = 1,
            float_: float = 1.0,
            str_: str = "1",
            bool_: bool = True,
            list_: list = [1],
            tuple_: tuple = (1,),
            dict_: dict = {1: 1},
            set_: set = {1},
            Any_: Any = 1,
        ):
            pass

        for key, value in inspect.signature(test_function).parameters.items():
            type_str = get_types_from_signature(value)
            self.assertEqual(key.split("_")[0], type_str)

    def test_without_annotation_but_defaults(self):
        def test_function(
            int_=1,
            float_=1.0,
            str_="1",
            bool_=True,
            list_=[1],
            tuple_=(1,),
            dict_={1: 1},
            set_={1},
        ):
            pass

        for key, value in inspect.signature(test_function).parameters.items():
            type_str = get_types_from_signature(value)
            self.assertEqual(key.split("_")[0], type_str)

    def test_optional_types(self):
        def test_function(
            int_: Optional[int],
            float_: Optional[float],
            str_: Optional[str],
            bool_: Optional[bool],
            list_: Optional[list],
            tuple_: Optional[tuple],
            dict_: Optional[dict],
            set_: Optional[set],
        ):
            pass

        for key, value in inspect.signature(test_function).parameters.items():
            type_str = get_types_from_signature(value)
            self.assertEqual(f"Union[{key.split('_')[0]}, None]", type_str)

    def test_union_types(self):
        def test_function(
            int_float: Union[int, float],
            int_str: Union[int, str],
            int_bool: Union[int, bool],
            int_list: Union[int, list],
            int_tuple: Union[int, tuple],
            int_dict: Union[int, dict],
            int_set: Union[int, set],
            str_float: Union[str, float],
            str_bool: Union[str, bool],
            bool_float: Union[bool, float],
        ):
            pass

        for key, value in inspect.signature(test_function).parameters.items():
            type_str = get_types_from_signature(value)
            self.assertEqual(f"Union[{key.split('_')[0]}, {key.split('_')[1]}]", type_str)

    def test_union_with_any(self):
        def test_function(
            int_any: Any,
            float_any: Any,
            str_any: Any,
            bool_any: Any,
            list_any: Any,
            tuple_any: Any,
            dict_any: Any,
            set_any: Any,
            none_any: Any,
        ):
            pass

        for key, value in inspect.signature(test_function).parameters.items():
            type_str = get_types_from_signature(value)
            self.assertEqual("Any", type_str)

    def test_nested_nested_types(self):
        test_cases = {
            0: {
                "function_value": Union[int, float],
                "eval_str": "Union[int, float]",
            },
            1: {
                "function_value": Union[Union[int, float], str],
                "eval_str": "Union[int, float, str]",
            },
            2: {
                "function_value": Union[Union[Union[int, float], str], bool],
                "eval_str": "Union[int, float, str, bool]",
            },
            3: {
                "function_value": Optional[Union[int, float, str, bool, list]],
                "eval_str": "Union[int, float, str, bool, list, None]",
            },
        }

        for key, value in test_cases.items():
            function_value = value["function_value"]
            eval_str = value["eval_str"]

            def test_function(
                a: function_value,
            ):
                pass

            for key, value in inspect.signature(test_function).parameters.items():
                type_str = get_types_from_signature(value)
                self.assertEqual(eval_str, type_str)

    def test_convert_arbitrary_type_into_any(self):
        def test_function(
            a: Dataset,
        ):
            pass

        for key, value in inspect.signature(test_function).parameters.items():
            type_str = get_types_from_signature(value)
            self.assertEqual("Any", type_str)

    def test_with_empty_string(self):
        def test_function(a="", b: str = "", c="  "):
            pass

        for key, value in inspect.signature(test_function).parameters.items():
            type_str = get_types_from_signature(value)
            self.assertEqual("str", type_str)

    def test_special_real_world_cases(self):
        test_cases = {
            0: {
                "function_value": Union[type[torch.utils.data.datapipes.datapipe.IterDataPipe], Callable[[], bool]],
                "eval_str": "Any",
            },
        }

        for key, value in test_cases.items():
            function_value = value["function_value"]
            eval_str = value["eval_str"]

            def test_function(
                a: function_value,
            ):
                pass

            for key, value in inspect.signature(test_function).parameters.items():
                type_str = get_types_from_signature(value)
                self.assertEqual(eval_str, type_str)
