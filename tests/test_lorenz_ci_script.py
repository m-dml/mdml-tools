import os
import tempfile
import unittest

import h5py
import torch

from mdml_tools.scripts import generate_lorenz_data


class TestDataGeneration(unittest.TestCase):
    def setUp(self) -> None:
        self.temp_dir = tempfile.TemporaryDirectory()
        self.temp_dir_name = self.temp_dir.name
        self.parser = generate_lorenz_data.create_parser()
        self.arg_dict = {
            "output-dir": self.temp_dir_name,
            "num-workers": 2,
            "num-trajectories": 3,
            "grid-size": 4,
            "dt": 0.01,
            "seed": 42,
            "spin-up-steps": 5,
            "num-steps": 6,
            "batch-size": 2,
            "lorenz-level": "one_level",
        }
        self.arg_list = [f"--{key}={value}" for key, value in self.arg_dict.items()]

    def test_0_generate_data(self):
        # parse the arguments
        parsed_args = self.parser.parse_args(self.arg_list)
        out_file = generate_lorenz_data.generate_data(parsed_args)

        # check if the data was created
        self.assertTrue(os.path.exists(out_file))

    def test_1_load_one_level_data(self):
        # parse the arguments
        parsed_args = self.parser.parse_args(self.arg_list)
        out_file = generate_lorenz_data.generate_data(parsed_args)

        # check if the data was created
        self.assertTrue(os.path.exists(out_file))

        # load the data
        with h5py.File(out_file, "r") as f:
            # check if the saved attributes are correct
            for key, value in self.arg_dict.items():
                self.assertEqual(f.attrs.get(key.replace("-", "_")), value)

            # check for dimension names
            self.assertEqual(f["first_level"].dims[0].label, "sample")
            self.assertEqual(f["first_level"].dims[1].label, "time")
            self.assertEqual(f["first_level"].dims[2].label, "grid")

            data = f["first_level"][:]

        # check if the data has the correct shape
        self.assertEqual(data.shape, (3, 6, 4))

    def test_2_load_two_level_data(self):
        # parse the arguments
        arg_dict = self.arg_dict.copy()
        arg_dict["lorenz-level"] = "two_level"
        arg_dict["device"] = "cpu"
        arg_dict["stepping-scheme"] = "dopri5"
        arg_dict["y-grid-size"] = 10
        arg_list = [f"--{key}={value}" for key, value in arg_dict.items()]
        parsed_args = self.parser.parse_args(arg_list)

        out_file = generate_lorenz_data.generate_data(parsed_args)

        # check if the data was created
        self.assertTrue(os.path.exists(out_file))

        # load the data
        with h5py.File(out_file, "r") as f:
            # check if the saved attributes are correct
            for key, value in arg_dict.items():
                self.assertEqual(f.attrs.get(key.replace("-", "_")), value)

            # check that there are 2 datasets
            self.assertEqual(len(f.keys()), 2)

            # check for dimension names

            self.assertEqual(f["first_level"].dims[0].label, "sample")
            self.assertEqual(f["first_level"].dims[1].label, "time")
            self.assertEqual(f["first_level"].dims[2].label, "grid")

            self.assertEqual(f["second_level"].dims[0].label, "sample")
            self.assertEqual(f["second_level"].dims[1].label, "time")
            self.assertEqual(f["second_level"].dims[2].label, "grid")

            data_first_level = f["first_level"][:]
            data_second_level = f["second_level"][:]

        # check if the data has the correct shape
        self.assertEqual(data_first_level.shape, (3, 6, 4))
        self.assertEqual(data_second_level.shape, (3, 6, 4, 10))

    def test_generation_on_GPU(self):
        if not torch.cuda.is_available():
            raise unittest.SkipTest("CUDA not available")

        # parse the arguments
        arg_dict = self.arg_dict.copy()
        arg_dict["lorenz-level"] = "two_level"
        arg_dict["device"] = "cuda"
        arg_dict["stepping-scheme"] = "dopri5"
        arg_dict["y-grid-size"] = 10
        arg_list = [f"--{key}={value}" for key, value in arg_dict.items()]
        parsed_args = self.parser.parse_args(arg_list)

        out_file = generate_lorenz_data.generate_data(parsed_args)

        # check if the data was created
        self.assertTrue(os.path.exists(out_file))

        # load the data
        with h5py.File(out_file, "r") as f:
            # check if the saved attributes are correct
            for key, value in arg_dict.items():
                self.assertEqual(f.attrs.get(key.replace("-", "_")), value)

            # check that there are 2 datasets
            self.assertEqual(len(f.keys()), 2)

            # check for dimension names

            self.assertEqual(f["first_level"].dims[0].label, "sample")
            self.assertEqual(f["first_level"].dims[1].label, "time")
            self.assertEqual(f["first_level"].dims[2].label, "grid")

            self.assertEqual(f["second_level"].dims[0].label, "sample")
            self.assertEqual(f["second_level"].dims[1].label, "time")
            self.assertEqual(f["second_level"].dims[2].label, "grid")

            data_first_level = f["first_level"][:]
            data_second_level = f["second_level"][:]

        # check if the data has the correct shape
        self.assertEqual(data_first_level.shape, (3, 6, 4))
        self.assertEqual(data_second_level.shape, (3, 6, 4, 10))
