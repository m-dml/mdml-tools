import unittest

import hydra
import torch
from hydra.core.config_store import ConfigStore
from torch import nn

from mdml_tools.utils import add_hydra_models_to_config_store


class TestFullyConnectedModel(unittest.TestCase):
    def setUp(self) -> None:
        torch.manual_seed(4)
        cs = ConfigStore.instance()
        cs = add_hydra_models_to_config_store(cs)
        self.cs = cs

    def test_training(self):
        activation = nn.ReLU()
        model = hydra.utils.instantiate(
            self.cs.repo["model"]["fully_connected_model_base.yaml"].node,
            activation=activation,
            num_classes=1,
            input_features=1,
            hidden_layers=[2],
        )
        self.assertIsNotNone(model)
        optimizer = hydra.utils.instantiate(
            self.cs.repo["optimizer"]["adam_base.yaml"].node,
            params=model.parameters(),
        )
        self.assertIsNotNone(optimizer)
        loss = nn.MSELoss()
        data = torch.full(size=(10000, 64, 1), fill_value=1.0)

        for batch_data in data:
            optimizer.zero_grad()
            output = model(batch_data)
            loss_value = loss(output, batch_data)
            loss_value.backward()
            optimizer.step()

        self.assertAlmostEqual(loss_value.item(), 0.0, places=2)
