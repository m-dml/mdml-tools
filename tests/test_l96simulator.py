from unittest import TestCase

import numpy as np
import torch

from mdml_tools.scripts.generate_lorenz_data import get_initial_condition_x, get_initial_condition_y
from mdml_tools.simulators import L96Simulator
from mdml_tools.simulators.lorenz96 import L96SimulatorOneLevel, L96SimulatorTwoLevel


class TestLorenz96Simulator(TestCase):
    def test_one_level_l96(self):
        k, f = 40, 8.0
        n_steps, spin_up_steps, dt = 500, 300, 0.01
        model = L96SimulatorOneLevel(f)
        grid = torch.Size((1, 1, k))
        x_init = f * (0.5 + torch.randn(grid, device="cpu") * 1.0)
        t = torch.arange(0, dt * (n_steps + spin_up_steps), dt)
        x = model.integrate(t, x_init)[:, spin_up_steps:, :]
        self.assertEqual(x.size(), torch.Size((1, n_steps, k)))

    def test_two_level_l96(self):
        k, j, f = 40, 10, 8.0
        n_steps, spin_up_steps, dt = 500, 300, 0.01

        model = L96SimulatorTwoLevel(f)
        grid = torch.Size((1, k))
        x_init = f * (0.5 + torch.randn(grid, device="cpu") * 1.0) / torch.tensor([j, 50]).max()
        y_init = f * (0.5 + torch.randn(torch.Size((1, k, j)), device="cpu") * 1.0) / torch.tensor([j, 50]).max()
        t = torch.arange(0, dt * (n_steps + spin_up_steps), dt)
        x, y = model.integrate(t, (x_init, y_init))
        x = x[:, spin_up_steps:, ...]
        y = y[:, spin_up_steps:, ...]
        self.assertEqual(x.size(), torch.Size((1, n_steps, k)))
        self.assertEqual(y.size(), torch.Size((1, n_steps, k, j)))

    def test_init_using_wrapper_function(self):
        k, j, f = 40, 10, 8.0
        n_steps, spin_up_steps, dt = 500, 300, 0.01
        model = L96Simulator("two_level", forcing=f)
        grid = torch.Size((1, k))
        x_init = f * (0.5 + torch.randn(grid, device="cpu") * 1.0) / torch.tensor([j, 50]).max()
        y_init = f * (0.5 + torch.randn(torch.Size((1, k, j)), device="cpu") * 1.0) / torch.tensor([j, 50]).max()
        t = torch.arange(0, dt * (n_steps + spin_up_steps), dt)
        x, y = model.integrate(t, (x_init, y_init))
        x = x[:, spin_up_steps:, ...]
        y = y[:, spin_up_steps:, ...]
        self.assertEqual(x.size(), torch.Size((1, n_steps, k)))
        self.assertEqual(y.size(), torch.Size((1, n_steps, k, j)))

    def test_one_level_batched_vs_single(self):
        k, f = 40, 8.0
        n_steps, spin_up_steps, dt = 100, 300, 0.01
        model = L96SimulatorOneLevel(f)
        grid = torch.Size((5, 1, k))
        x_init = f * (0.5 + torch.randn(grid, device="cpu") * 1.0)
        t = torch.arange(0, dt * (n_steps + spin_up_steps), dt)
        x_batched = model.integrate(t, x_init)[:, spin_up_steps:, :].squeeze()
        x_single = torch.stack([model.integrate(t, x_init[i, ...])[:, spin_up_steps:, :] for i in range(5)]).squeeze()
        self.assertEqual(x_batched.size(), x_single.size())
        self.assertTrue(torch.allclose(x_batched, x_single))

    def test_two_level_batched_vs_single(self):
        k, j, f = 40, 10, 8.0
        n_steps, spin_up_steps, dt = 100, 300, 0.01
        batch_size = 5

        model = L96SimulatorTwoLevel(f)

        x_init = torch.stack([get_initial_condition_x(forcing=f, grid_size=k, device="cpu") for _ in range(batch_size)])
        y_init = torch.stack(
            [get_initial_condition_y(forcing=f, grid_size=k, y_grid_size=j, device="cpu") for _ in range(batch_size)]
        )
        t = torch.arange(0, dt * (n_steps + spin_up_steps), dt)
        x_batched, y_batched = model.integrate(t, (x_init, y_init))
        x_batched = x_batched[:, spin_up_steps:, ...].squeeze()
        y_batched = y_batched[:, spin_up_steps:, ...].squeeze()

        x_single = []
        y_single = []
        for i in range(batch_size):
            _x, _y = model.integrate(t, (x_init[i, ...], y_init[i, ...]))
            x_single.append(_x[:, spin_up_steps:, ...])
            y_single.append(_y[:, spin_up_steps:, ...])
        x_single = torch.stack(x_single).squeeze()
        y_single = torch.stack(y_single).squeeze()

        self.assertEqual(x_batched.size(), x_single.size())
        self.assertEqual(y_batched.size(), y_single.size())
        self.assertTrue(torch.allclose(x_batched, x_single))
        self.assertTrue(torch.allclose(y_batched, y_single))

    def test_one_level_stationary_case(self):
        """For the one level lorenz96 model, we now there is an attractor for K=24 and F=2.75. So no matter what the
        initial conditions are, at some point in the far future the model should only oscillate around the attractor.

        See https://pure.rug.nl/ws/portalfiles/portal/65106872/Complete_thesis.pdf figure 1.4 for an example.

        This test checks if the model is able to reproduce this behavior.
        """
        torch.manual_seed(42)
        np.random.seed(42)
        k, f = 24, 2.75
        n_steps, spin_up_steps, dt = 10000, 1000, 0.01
        model = L96SimulatorOneLevel(f)
        grid = torch.Size((1, 1, k))
        x_init = f * (0.5 + torch.randn(grid, device="cpu") * 1.0)
        t = torch.arange(0, dt * (n_steps + spin_up_steps), dt)
        x_batched = model.integrate(t, x_init)[:, spin_up_steps:, :].squeeze()  # [1, 10000, 24] -> [10000, 24]

        pattern_to_find = x_batched[-100:, :]
        n_pattern_found = 0

        for i in range(10000 - 100):
            if torch.allclose(x_batched[i : i + 100, :], pattern_to_find, atol=0.05):
                n_pattern_found += 1

        print("Found the pattern {} times.".format(n_pattern_found))
        self.assertGreater(n_pattern_found, 10)
