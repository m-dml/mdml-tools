import os
import unittest
from tempfile import TemporaryDirectory

import pandas as pd
from torch.utils.tensorboard import SummaryWriter

from mdml_tools.utils import read_scalars_from_tensorboard


class TestTensorboardParser(unittest.TestCase):
    def setUp(self) -> None:
        self.tmp_dir = TemporaryDirectory()
        self.tmp_file_path = os.path.join(self.tmp_dir.name, "test")

        # generate test tensorboard log
        test_summary_writer = SummaryWriter(self.tmp_file_path)
        test_summary_writer.add_scalar("Loss", 0.345, 1)
        test_summary_writer.add_scalar("Loss", 0.234, 2)
        test_summary_writer.add_scalar("Loss", 0.123, 3)
        test_summary_writer.close()

    def test_read_scalars_from_tensorboard(self):
        scalars = read_scalars_from_tensorboard(self.tmp_file_path)
        self.assertIsInstance(scalars, dict)
        self.assertIsInstance(scalars["Loss"], pd.DataFrame)
        self.assertEqual(len(scalars["Loss"]), 3)
        self.assertEqual(scalars["Loss"]["step"].tolist(), [1, 2, 3])

    def tearDown(self):
        self.tmp_dir.cleanup()
