"""Module containing a custom resnet where the kernel size of the first convolutional layer can be changed."""


import torch
from torch import nn


class CustomResnet(nn.Module):
    """Wrapper for a torch resnet where the kernel size of the first convolutional layer can be changed and maxpooling
    can be disabled.

    It can be called with a config file like this

        .. code-block:: yaml
            :caption: resnet18.yaml

            defaults:
                - custom_resnet_base
                - _self_

            model:
                _target_: "torchvision.models.resnet18"
                num_classes: 1000


    Args:
        model (torch.nn.Module): Original resnet model.
        kernel_size (int, optional): Kernel size of the first convolutional layer. Defaults to 7.
        stride (int, optional): Stride of the first convolutional layer. Defaults to 2.
        channels (int, optional): Number of channels of the input tensor. Defaults to 3.
        maxpool1 (bool, optional): Whether to use max-pooling after the first convolutional layer. Defaults to True.

    Example:

        .. code-block:: python

            import hydra
            import torch
            from mdml_tools.hydra_models.models import Model

            resnet = torch.hub.load('pytorch/vision:v0.6.0', 'resnet18', pretrained=False)
            model: Model = hydra.utils.instantiate(model=resnet kernel_size=3, stride=1, channels=)
    """

    def __init__(
        self, model: torch.nn.Module, kernel_size: int = 7, stride: int = 2, channels: int = 3, maxpool1: bool = True
    ):
        super().__init__()
        self.model = model

        if not maxpool1:
            self.model.maxpool = nn.MaxPool2d(kernel_size=1, stride=1, padding=0)

        conv1_out_channels = self.model.conv1.out_channels
        self.model.conv1 = nn.Conv2d(
            channels, conv1_out_channels, kernel_size=kernel_size, stride=stride, padding=3, bias=False
        )

    def forward(self, image_tensor: torch.Tensor) -> torch.Tensor:
        """Forward pass of the resnet wrapper to the original resnet.

        Args:
            image_tensor (torch.Tensor): Input tensor.
        """
        return self.model(image_tensor)
