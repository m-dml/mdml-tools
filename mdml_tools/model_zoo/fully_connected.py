"""Module containing a fully customizable fully connected network."""

from collections.abc import Iterable

import torch
import torch.nn.functional as F
from torch import nn


class FullyConnectedModel(nn.Module):
    """A fully connected model with a variable number of fully connected hidden layers and batch normalization.

    Args:
        num_classes (int): Number of classes.
        hidden_layers (Iterable): Iterable containing the number of nodes in each hidden layer. len(hidden_layer)
            therefore determines the number of hidden layers. If an empty iterable is passed, no hidden layers are
            used. Automatically, no matter hwo many hidden layers are used, a final output layer without activation
            is added.
        activation (nn.Module): Activation function to be used between the hidden layers.
        input_features (int): Number of input features. If the classifier is used together with a feature extractor,
            this should be the number of output features of the feature extractor.
        normalize (bool): If True, the output of the classifier is normalized using torch.nn.functional.normalize().
        bias_in_last_layer (bool): If True, the last layer has a bias. If False, the last layer has no bias.
            Default is True.
        use_batch_norm (bool): If True, batch norm layers are going to be added after each linear layer except the
            last one. If False, batch norm is going to be ignored.
            Default is False.

    Example:
        To construct a classifier with 2 hidden layers with 1000 nodes each, one input and one output feature, one can:

        .. code-block:: python

            from mdml_tools.model_zoo.fully_connected import FullyConnectedModel
            classifier = FullyConnectedModel(num_classes=1, input_feature=1, hidden_layers=(1000, 1000))
    """

    def __init__(
        self,
        num_classes: int,
        hidden_layers: Iterable = (1000, 1000),
        activation=nn.ReLU(),
        input_features: int = 1000,
        normalize: bool = False,
        bias_in_last_layer: bool = True,
        use_batch_norm: bool = False,
    ):
        super().__init__()
        self.normalize = normalize
        self.hidden_layers = list(hidden_layers)
        self.num_classes = num_classes
        self.activation = activation
        self.hidden_layers.insert(0, input_features)
        self.use_batch_norm = use_batch_norm

        modules = []
        for i in range(len(self.hidden_layers) - 1):
            modules.append(nn.Linear(self.hidden_layers[i], self.hidden_layers[i + 1]))
            if self.use_batch_norm:
                modules.append(nn.BatchNorm1d(self.hidden_layers[i + 1]))
            modules.append(activation)

        modules.append(nn.Linear(self.hidden_layers[-1], num_classes, bias=bias_in_last_layer))
        self.model = nn.Sequential(*modules)

    def forward(self, input_tensor) -> torch.Tensor:
        """Forward pass of the classifier.

        Args:
            input_tensor (torch.Tensor): Input tensor.
        """
        if self.normalize:
            input_tensor = self.model(input_tensor)
            return F.normalize(input_tensor, dim=1)

        return self.model(input_tensor)
