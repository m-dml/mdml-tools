"""Command line script to generate Lorenz data.

We make use of the simulators package of mdml_tools to create tailored training data for machine learning models. The
outputs will be saved in the hdf5 format.

Example:

    .. code-block:: bash

        python generate_lorenz_data.py --output-dir ./data --seed 42 --num-workers 1 --spin-up-steps 1000 \
            --num-trajectories 1000 --num-steps 1000 --dt 0.01 --forcing 8.0 --batch-size 1 --grid-size 40

This will generate 1000 independent trajectories of the Lorenz 96 model.

To load the data, you can use the following snippet:

    .. code-block:: python

        import h5py
        import torch

        with h5py.File("./data/data.h5", "r") as f:
            data = f["data"][:]
            forcing = f.attrs.get("forcing")
            dt = f.attrs.get("dt")
            grid_size = f.attrs.get("grid_size")

        tensor_data = torch.from_numpy(data)
"""

import argparse
import logging
import os
import time

import h5py
import numpy as np
import torch
from torch.utils.data import DataLoader, Dataset
from tqdm import tqdm

from mdml_tools.simulators.lorenz96 import L96Simulator, L96SimulatorTwoLevel
from mdml_tools.utils import logging as mdml_logging


def get_initial_condition_x(forcing, grid_size, device):
    return forcing * (0.5 + torch.randn(torch.Size((1, 1, grid_size)), device=device) * 1.0)


def get_initial_condition_y(forcing, grid_size, y_grid_size, device):
    ic = forcing * (0.5 + torch.randn(torch.Size((1, 1, grid_size, y_grid_size)), device=device) * 1.0)
    return ic / torch.max(torch.tensor([y_grid_size, 50]))


class LorenzDataset(Dataset):
    """Dataset class for the Lorenz 96 model.

    Args:
        simulator: Simulator object for the Lorenz 96 model.
        n_samples: Number of samples to generate.
        spin_up_steps: Number of spin-up steps to use for generating independent trajectories.
        dt: Time step size to use for generating the data.
        grid_size: Grid size of the Lorenz 96 model.
        y_grid_size: Grid size of the second level of the Lorenz 96 model.
        forcing: Forcing of the Lorenz 96 model.
        n_steps: Number of steps to integrate the Lorenz 96 model.
        initial_condition_x: Initial condition to use for generating the data. If None, a random initial condition will
            be used.
        initial_condition_y: Initial condition to use for generating the second level data. If None, a random initial
            condition will be used.
    """

    def __init__(
        self,
        simulator: L96Simulator,
        n_samples: int,
        grid_size: int,
        y_grid_size: int,
        forcing: float,
        initial_condition_x: torch.Tensor = None,
        initial_condition_y: torch.Tensor = None,
        device: str = "cpu",
    ):
        self.simulator = simulator
        self.n_samples = n_samples
        self.grid_size = grid_size
        self.y_grid_size = y_grid_size
        self.forcing = forcing
        self.initial_condition_x = initial_condition_x
        self.initial_condition_y = initial_condition_y
        self.device = device

    def __len__(self) -> int:
        return self.n_samples

    def __getitem__(self, *args, **kwargs) -> tuple[torch.Tensor, torch.Tensor]:
        if self.initial_condition_x is None:
            x_init = get_initial_condition_x(self.forcing, self.grid_size, self.device)
        else:
            x_init = self.initial_condition_x

        if isinstance(self.simulator, L96SimulatorTwoLevel):
            x_init /= torch.max(torch.tensor([self.y_grid_size, 50]))
            if self.initial_condition_y is None:
                y_init = get_initial_condition_y(self.forcing, self.grid_size, self.y_grid_size, self.device)
            else:
                y_init = self.initial_condition_y
            return x_init, y_init

        return x_init, -777


def create_parser() -> argparse.ArgumentParser:
    """Creating the argument parser and adding the arguments.

    Valid arguments are:
        --output-dir: Path to the output directory.
        --seed: Seed to use for generating the data.
        --num-workers: Number of workers to use for generating the data.
        --spin-up-steps: Number of spin-up steps to use for generating independent trajectories.
        --num-trajectories: Number of independent trajectories to generate.
        --num-steps: Number of steps to generate for each trajectory.
        --dt: Time step size to use for generating the data.
        --forcing: Forcing for the Lorenz 96 model.
        --batch-size: Batch size to use for generating the data. This has nothing to do with the batch size used in
            training later.
        --grid-size: Grid size to use for generating the data.
        --overwrite: Whether to overwrite the output directory if it already exists.
        --lorenz-level: Number of levels to use for the Lorenz 96 model (either "one", or "two").
        --y-grid-size: Grid size to use for the second level of the Lorenz 96 model.
        --b: Parameter b to use for the second level of the Lorenz 96 model.
        --c: Parameter c to use for the second level of the Lorenz 96 model.
        --h: Parameter h to use for the second level of the Lorenz 96 model.
        --stepping-scheme: Stepping scheme to use for the second level of the Lorenz 96 model.
        --device: Device to use for generating the data.
    """
    parser = argparse.ArgumentParser(
        description="Create Lorenz96 simulator data. Each trajectory is randomly initialized and the model is spun up "
        "for a certain number of steps before the actual trajectory is generated. Outputs are saved in the hdf5 format."
    )
    parser.add_argument("--output-dir", type=str, help="Path to the output directory.")
    parser.add_argument("--seed", type=int, help="Seed to use for generating the data.", default=42)
    parser.add_argument(
        "--num-workers",
        type=int,
        help="Number of workers to use for generating initial states for the data. "
        "The actual integration is done in parallel (batched) on a single device.",
        default=1,
    )
    parser.add_argument(
        "--spin-up-steps",
        type=int,
        help="Number of spin-up steps to use for generating independent trajectories.",
        default=1000,
    )
    parser.add_argument(
        "--num-trajectories", type=int, help="Number of independent trajectories to generate.", default=100
    )
    parser.add_argument("--num-steps", type=int, help="Number of steps to generate for each trajectory.", default=1000)
    parser.add_argument("--dt", type=float, help="Time step size to use for generating the data.", default=0.01)
    parser.add_argument("--forcing", type=float, help="Forcing for the Lorenz 96 model.", default=8.0)
    parser.add_argument(
        "--batch-size",
        type=int,
        help="Batch size to use for generating the data. This has nothing to do with the batch size used in training "
        "later.",
        default=1,
    )
    parser.add_argument("--grid-size", type=int, help="Grid size to use for generating the data.", default=40)
    parser.add_argument(
        "--y-grid-size",
        type=int,
        help="Grid size to use for the fast veriables in case of a two_level lorenz model. This is ignored if the "
        "one-level model is used.",
        default=10,
    )
    parser.add_argument(
        "--overwrite", action="store_true", help="Whether to overwrite the output directory if it already exists."
    )
    parser.add_argument(
        "--lorenz-level",
        type=str,
        help="Number of levels to use for the Lorenz 96 model (either 'one_level', or 'two_level').",
        default="one_level",
    )
    parser.add_argument("--stepping-scheme", default="rk4", help="Stepping scheme to use for the integrator.")
    parser.add_argument(
        "--b",
        type=float,
        default=10.0,
        help="Parameter b for the two-level Lorenz 96 model. This is ignored if the one-level model is used.",
    )
    parser.add_argument(
        "--c",
        type=float,
        default=1.0,
        help="Parameter c for the two-level Lorenz 96 model. This is ignored if the one-level model is used.",
    )
    parser.add_argument(
        "--h",
        type=float,
        default=10.0,
        help="Parameter h for the two-level Lorenz 96 model. This is ignored if the one-level model is used.",
    )
    parser.add_argument(
        "--device",
        type=str,
        default="cpu",
        help="Device to use for generating the data. Can be any string that is accepted by torch.device().",
    )

    return parser


def parse_args(parser: argparse.ArgumentParser) -> argparse.Namespace:
    """Convert the arguments to a namespace and check if they are valid.

    Args:
        parser (argparse.ArgumentParser): The argument parser.

    Returns:
        (str): The parsed arguments.
    """
    logger = mdml_logging.get_logger()
    args = parser.parse_args()
    output_file = os.path.join(
        args.output_dir,
        f"lorenz-{args.lorenz_level}-grid_{args.grid_size}-forcing_{args.forcing}-dt_{args.dt}-"
        f"{args.stepping_scheme}.h5",
    )
    if os.path.exists(output_file):
        if args.overwrite:
            logger.warning(f"The output file {output_file} already exists and will be overwritten.")
        else:
            raise ValueError(
                f"The output directory {output_file} already exists. Please specify a different output directory "
                "or use the --overwrite flag."
            )
    else:
        os.makedirs(args.output_dir, exist_ok=True)

    if args.lorenz_level not in ["one_level", "two_level"]:
        raise ValueError(
            f"The Lorenz level {args.lorenz_level} is not valid. Please choose either 'one_level' or 'two_level'."
        )

    try:
        torch.device(args.device)
    except Exception as e:
        raise ValueError(f"Could not parse the device {args.device}.") from e

    return args


def write_to_file(x, y, level, int_batch_size, i, f):
    f["first_level"][i * int_batch_size : (i + 1) * int_batch_size] = x.squeeze().cpu().numpy()

    if level == "two_level":
        f["second_level"][i * int_batch_size : (i + 1) * int_batch_size] = y.squeeze().cpu().numpy()


def integrate_and_write_to_file(x_init, y_init, simulator, t, spin_up_steps, level, int_batch_size, i, f):
    if level == "one_level":
        x = simulator.integrate(t, x_init)[:, spin_up_steps:, :]
        y = None

    else:
        x, y = simulator.integrate(t, (x_init, y_init))
        x = x[:, spin_up_steps:, :]
        y = y[:, spin_up_steps:, :]

    write_to_file(x=x, y=y, level=level, int_batch_size=int_batch_size, i=i, f=f)
    return x, y


def check_data_integrity(x_init, y_init, x, y, simulator, t, spin_up_steps, level, int_batch_size, i, f, logger):
    # assert all data is written to the file and nothing is nan:
    if np.isnan(f["first_level"][i * int_batch_size : (i + 1) * int_batch_size]).any():
        recalc_batch = False
        if np.isnan(x_init).any():
            logger.warning("x_init contains nans. Recalculating batch.")
            recalc_batch = True
        if np.isnan(x).any():
            logger.warning("x contains nans. Recalculating batch.")
            recalc_batch = True

        if level == "two_level":
            if np.isnan(y_init).any():
                logger.warning("y_init contains nans. Recalculating batch.")
                recalc_batch = True
            if np.isnan(y).any():
                logger.warning("y contains nans. Recalculating batch.")
                recalc_batch = True

        if recalc_batch:
            # create new initial conditions in any case:
            batch_size = x_init.shape[0]
            grid_size = x_init.shape[-1]
            y_grid_size = y_init.shape[-1]
            x_init = torch.stack(
                list(
                    map(
                        lambda _: get_initial_condition_x(simulator.forcing, grid_size, x_init.device),
                        range(batch_size),
                    )
                ),
                dim=0,
            )
            y_init = torch.stack(
                list(
                    map(
                        lambda _: get_initial_condition_y(simulator.forcing, grid_size, y_grid_size, y_init.device),
                        range(batch_size),
                    )
                ),
                dim=0,
            )

            # redo the batch:
            x, y = integrate_and_write_to_file(
                x_init=x_init,
                y_init=y_init,
                simulator=simulator,
                t=t,
                spin_up_steps=spin_up_steps,
                level=level,
                int_batch_size=int_batch_size,
                i=i,
                f=f,
            )

        else:
            logger.info("x and y do not contain nans. Attempting to writing to file again.")
            # close and reopen the file:
            f.close()
            time.sleep(1)
            f = h5py.File(f.filename, "r+")
            time.sleep(1)
            write_to_file(x=x, y=y, level=level, int_batch_size=int_batch_size, i=i, f=f)

        return False
    return True


def generate_data(args: argparse.Namespace) -> str:
    """Entry point for generating the data.

    Args:
        args (argparse.Namespace): The parsed arguments.

    Returns:
        (str): The path to the generated data.
    """
    logger = mdml_logging.get_logger()
    logger.setLevel("INFO")
    logger.addHandler(logging.StreamHandler())

    # Set the seed:
    torch.manual_seed(args.seed)

    logger.info(f"Generating Simulator for the Lorenz 96 model with {args.lorenz_level} levels.")
    simulator = L96Simulator(
        forcing=args.forcing,
        simulator_type=args.lorenz_level,
        method=args.stepping_scheme,
        b=args.b,
        c=args.c,
        h=args.h,
    )

    # Create the dataset:
    logger.info("Creating Dataset.")
    dataset = LorenzDataset(
        simulator=simulator,
        n_samples=args.num_trajectories,
        grid_size=args.grid_size,
        forcing=args.forcing,
        y_grid_size=args.y_grid_size,
        device="cpu",
    )

    # Create the data loader:
    logger.info("Creating DataLoader.")
    data_loader = DataLoader(
        dataset, batch_size=args.batch_size, shuffle=False, num_workers=args.num_workers, drop_last=False
    )

    # Create the output file:
    output_file = os.path.join(
        args.output_dir,
        f"lorenz-{args.lorenz_level}-grid_{args.grid_size}-forcing_{args.forcing}-dt_{args.dt}-"
        f"{args.stepping_scheme}.h5",
    )
    logger.info(f"Writing data to {output_file}.")

    # Write the data to the output file:
    file_handle = h5py.File(output_file, "w")
    logger.info("Saving Metadata to hdf5 file.")
    # save metadata:
    for k, v in vars(args).items():
        file_handle.attrs[k] = v

    file_handle.create_dataset(
        "first_level", shape=(args.num_trajectories, args.num_steps, args.grid_size), dtype=np.float32
    )
    level_names = ["first_level"]

    if args.lorenz_level == "two_level":
        file_handle.create_dataset(
            "second_level",
            shape=(args.num_trajectories, args.num_steps, args.grid_size, args.y_grid_size),
            dtype=np.float32,
        )
        level_names.append("second_level")

    # add dimensions to dataset:
    for level in level_names:
        file_handle[level].dims[0].label = "sample"
        file_handle[level].dims[1].label = "time"
        file_handle[level].dims[2].label = "grid"

    logger.info("Starting to write data to hdf5 file.")
    int_batch_size = int(args.batch_size)

    run_length = np.ceil(args.num_trajectories / int_batch_size).astype(int)

    with torch.no_grad():
        t = torch.arange(0, args.dt * (args.num_steps + args.spin_up_steps), args.dt, device=args.device)
        spin_up_steps = args.spin_up_steps
        for i, (x_init, y_init) in tqdm(enumerate(data_loader), total=run_length):
            x_init = x_init.to(args.device)
            y_init = y_init.to(args.device)
            x, y = integrate_and_write_to_file(
                x_init=x_init,
                y_init=y_init,
                simulator=simulator,
                t=t,
                spin_up_steps=spin_up_steps,
                level=args.lorenz_level,
                int_batch_size=int_batch_size,
                i=i,
                f=file_handle,
            )
            data_is_ok = check_data_integrity(
                x_init=x_init,
                y_init=y_init,
                x=x,
                y=y,
                simulator=simulator,
                t=t,
                spin_up_steps=spin_up_steps,
                level=args.lorenz_level,
                int_batch_size=int_batch_size,
                i=i,
                f=file_handle,
                logger=logger,
            )

            data_ok_counter = 0
            while not data_is_ok:
                logger.warning(f"Data integrity check failed for batch {i}. Trying one more time")
                data_is_ok = check_data_integrity(
                    x_init=x_init,
                    y_init=y_init,
                    x=x,
                    y=y,
                    simulator=simulator,
                    t=t,
                    spin_up_steps=spin_up_steps,
                    level=args.lorenz_level,
                    int_batch_size=int_batch_size,
                    i=i,
                    f=file_handle,
                    logger=logger,
                )
                data_ok_counter += 1

                if data_ok_counter > 10:
                    raise ValueError(
                        f"Data integrity check failed for batch {i} 10 times. This is most likely "
                        f"due to the method {simulator.method} or step-size dt {args.dt} Aborting."
                    )

    file_handle.close()
    logger.info(f"Data written to {output_file}. There should be no nans in the data.")
    logger.info("Done.")

    return output_file


if __name__ == "__main__":
    _parser = create_parser()
    parsed_args = parse_args(_parser)
    generate_data(parsed_args)
