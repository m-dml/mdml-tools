"""Script to kill all processes on all nodes that belong to the user that executes this script.

The processes are killed directly on the executing machine, so there is no graceful shutdown of the processes.
"""

import subprocess
import time

import paramiko
from tqdm import tqdm

from mdml_tools.utils.logging import get_logger

if __name__ == "__main__":
    logger = get_logger(__name__)
    # get slurm username:
    username = subprocess.run("whoami", shell=True, stdout=subprocess.PIPE, check=False).stdout.decode("utf-8").strip()

    # find out which nodes are currently running jobs by the user:
    cmd_to_execute = "squeue -u " + username + " -h -o %N"
    running_jobs_str = subprocess.run(cmd_to_execute, shell=True, stdout=subprocess.PIPE, check=False)

    # convert the string of nodes to a list of nodes:
    running_jobs_list = running_jobs_str.stdout.decode("utf-8").strip().split("\n")
    running_jobs_list = [node for node in running_jobs_list if node != ""]

    # connect to each node and kill all python processes that belong to the executing user:
    for node in tqdm(running_jobs_list, desc="Killing python processes on all nodes"):
        ssh = paramiko.SSHClient()
        try:
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(node, username=username)
            stdin, stdout, stderr = ssh.exec_command("pkill -u " + username + " python")
            ssh.close()
            time.sleep(1)
        except Exception as exc:  # pylint: disable=broad-except
            logger.exception(f"Could not connect to node {node} to kill python processes.", exc_info=exc)
        finally:
            ssh.close()
