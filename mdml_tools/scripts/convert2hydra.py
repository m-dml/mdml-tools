"""This script can be used to convert a python module, class or function into a hydra config dataclass to be used in a
structured config approach."""

import ast
import collections.abc
import importlib
import inspect
import typing
from argparse import ArgumentParser
from copy import deepcopy
from dataclasses import dataclass
from enum import Enum
from typing import Any, Type, Union

import black
import hydra
from hydra.errors import HydraException
from omegaconf.errors import ConfigValueError, ValidationError

T: Type = list[Union[type, "T"]]  # recursive type alias for indefinite depth for lists of lists of lists of ...


def evaluate_import_str(import_str: str) -> Any:
    """Try to import the module given in the import_str.

    Args:
        import_str (str): String containing the module to import. For example,
            'mdml_tools.utils.read_scalars_from_tensorboard'.

    Returns:
        module: Base module from the import_str. if the input_str was "torch.utils.data" this function will return the
            "torch" module.
    """
    # try to import the target_module given in the import_str
    try:
        try:
            target_module = importlib.import_module(import_str)
        except ImportError:
            # try "from base import module"
            base_module = ".".join(import_str.split(".")[:-1])
            module = import_str.split(".")[-1]
            loaded_base_module = importlib.util.find_spec(base_module).loader.load_module()
            target_module = getattr(loaded_base_module, module)

    except ImportError as exc:
        raise ImportError(f"Could not import target_module {import_str}") from exc

    return target_module


def remove_unnecessary_list_dims(value: T, recursion_depth=0) -> T:
    """Remove unnecessary list dimensions from a list. For example, if the input is [[1, 2, 3]], the output will be.

    [1, 2, 3]. This will also remove list dimensions that contain only one element. For example, if the input is [[1],
    2, 3] the output will be [1, 2, 3]. Only the most outer list dimension will not be removed, so that the return type
    will always be a list.
    """
    out_value = deepcopy(value)
    if isinstance(value, list):
        if len(value) == 1:
            out_value = remove_unnecessary_list_dims(value[0], recursion_depth + 1)
        else:
            for i, element in enumerate(value):
                value[i] = remove_unnecessary_list_dims(element, recursion_depth + 1)
            out_value = value

    # make sure that the most outer list dimension is not removed:
    if recursion_depth == 0:
        if not isinstance(out_value, list):
            out_value = [out_value]
    return out_value


def from_each_list_and_sublist_remove_duplicates(value: T) -> list:
    """Remove duplicates from a list and all sublists.

    For example, if the input is [[1, 2, 3], [1, 2, 3]], the output will be [1, 2, 3]. If the input is [[1, 2], [2, 3]]
    the output will be [[1, 2], [2, 3]]. If the input is [1, 1] the output will be [1].
    """
    out_value = deepcopy(value)
    if isinstance(value, list):
        out_value = []
        for element in value:
            if element not in out_value:
                out_value.append(element)
        for i, element in enumerate(out_value):
            out_value[i] = from_each_list_and_sublist_remove_duplicates(element)
    return out_value


def get_str_type(_type: type) -> str:
    """Given any type, returns the string representation of the type as used in type-hints.

    For example, if the input is NoneType, the output will be 'None'.
    """
    if _type == type(None):  # noqa: E721
        return "None"
    if isinstance(_type, type(Any)):
        return "Any"
    if isinstance(_type, type(Enum)):
        return "Enum"

    return _type.__name__.lower()


def reduce_any_type_list(type_list: T) -> T:
    """Reduce a list of types that contains Any to a single Any.

    For example, if the input is [int, float, Any], the output will be Any, and Union[float, Union[int, Any]] will also
    be reduced to Any.
    """
    out_type_list = deepcopy(type_list)
    if isinstance(type_list, list):
        if len(type_list) == 1:
            out_type_list = reduce_any_type_list(type_list[0])
        else:
            for i, type_element in enumerate(type_list):
                type_list[i] = reduce_any_type_list(type_element)
            out_type_list = type_list

    if isinstance(out_type_list, list):
        if Any in out_type_list:
            out_type_list = Any
    return out_type_list


def get_types_from_signature(value: inspect.Parameter) -> str:
    """Get the type of a parameter from its signature.

    If the parameter has no type annotation, the default value will be used to determine the type. If the default value
    is not a valid Omegaconf type, the type will be set to Any.
    """
    # get the type of the parameter from its signature:

    def convert_type_to_omegaconf_native(_type_list: T) -> T:
        omegaconf_types = [str, int, float, bool, list, dict, tuple, set, type(None), Any, Enum]

        if isinstance(_type_list, tuple):
            _type_list = list(_type_list)
        for i, type_element in enumerate(_type_list):
            # if the element is a type(Callable), convert it to Any:
            if isinstance(type_element, typing._CallableGenericAlias):  # pylint: disable=protected-access
                _type_list[i] = Any
            elif len(typing.get_args(type_element)) > 1:
                _type_list[i] = convert_type_to_omegaconf_native(typing.get_args(type_element))
            elif isinstance(type_element, collections.abc.Sized) and not isinstance(type_element, str):
                if len(type_element) > 1:
                    _type_list[i] = convert_type_to_omegaconf_native(type_element)
            elif not any(type_element == omega_type for omega_type in omegaconf_types):
                _type_list[i] = Any

        for _ in range(20):
            # remove unnecessary lists:
            _type_list = remove_unnecessary_list_dims(_type_list)
            # remove duplicates:
            _type_list = from_each_list_and_sublist_remove_duplicates(_type_list)

        return _type_list

    def convert_to_string(_type_list: T) -> str:
        """Convert a list of types to a string.

        If the list contains other lists, the function will be called recursively and every sub-list will be enclosed in
        a Union.
        """
        if len(_type_list) == 1:
            _type_str = get_str_type(_type_list[0])
        else:
            _type_str = "Union["
            for type_element in _type_list:
                if isinstance(type_element, list):
                    _type_str += f"{convert_to_string(type_element)}, "
                else:
                    _type_str += get_str_type(type_element) + ", "

            # remove the last comma and space:
            _type_str = _type_str[:-2]
            _type_str += "]"

        return _type_str

    if value.annotation is inspect.Parameter.empty:
        if value.default is inspect.Parameter.empty:
            type_ = Any
        else:
            type_ = type(value.default)
    else:
        type_ = value.annotation

    if isinstance(type_, tuple):
        type_list = list(type_)
    else:
        type_list = [type_]
    type_list = convert_type_to_omegaconf_native(type_list)
    type_list = reduce_any_type_list(type_list)
    if not isinstance(type_list, list):
        type_list = [type_list]
    type_str = convert_to_string(type_list)

    # if the only type that is left is none, set it to Any:
    if type_str == "None":
        type_str = "Any"

    return type_str


def hydra_mock_init_function(value_to_be_initialized: Any, type_str: str):
    """Function to evaluate if the value with the given type can be initialized by hydra by actually instantiating it.

    Args:
        value_to_be_initialized: The value that should be initialized.
        type_: The type of the value that should be initialized as a string.
    """

    # convert the type string to a type:
    type_ = eval(type_str)

    # create a mock dataset class with the given value and type:
    @dataclass
    class MockDataset:
        value: type_ = value_to_be_initialized

    # register the mock dataset class with hydra:
    config_store = hydra.core.config_store.ConfigStore.instance()
    config_store.store(name="dataset", node=MockDataset, group="dataset")

    # try to instantiate the mock dataset class:
    elements = config_store.repo["dataset"]
    for element in elements.values():
        _: type_ = hydra.utils.instantiate(element.node)


def check_if_type_of_default_is_missing(type_str: str, default_value: Any) -> list:
    """Check if the type of the default value is missing in the type list.

    If the type of the default value is missing, it will be added to the type list.

    Args:
        type_str: The type list as a string.
        default_value: The default value of the parameter.
    """

    default_type = type(default_value)
    default_type_str = get_str_type(default_type)
    if default_type_str not in type_str and ("Any" not in type_str):
        type_str = f"Union[{type_str}, {default_type_str}]"

    return type_str


def validate_default_value(value: inspect.Parameter.default, type_str: str) -> Any:
    """Check the default value of a parameter and convert it into None, if it is not an allowed Omegaconf type."""

    try:
        # try to initialize the value with hydra:
        if value is not inspect.Parameter.empty:
            hydra_mock_init_function(value, type_str)
        else:
            raise HydraException
    except (HydraException, ValidationError, ConfigValueError):
        value = None

    type_str = check_if_type_of_default_is_missing(type_str, value)

    # if the value is a string add quotes:
    if isinstance(value, str):
        value = f'"{value}"'

    return value, type_str


def write_dataclass_to_file(
    output_py_file: str, name: str, obj: Any, sphinx_link_str: str, add_orig_docstring: bool = True
) -> str:
    """Write a dataclass to a python file.

    Args:
        output_py_file (str): The file to which the dataclass will be written.
        name (str): The name of the dataclass.
        obj (Any): The object for which the dataclass will be created. Needs to be a class or a function.
        sphinx_link_str (str): The string that will be added to the beginning of the link to the original object.
        add_orig_docstring (bool): If True, the docstring of the original object will be added to the dataclass.
    """
    output_py_file += f"""
@dataclass
class {name}:
    \"\"\"Hydra dataclass for :py:{sphinx_link_str}:`{obj.__module__}.{obj.__name__}`. """

    # add the docstring of the class or function to the dataclass:
    if add_orig_docstring:
        if obj.__doc__ is not None:
            if obj.__doc__.startswith("\n"):
                output_py_file += f"\n{obj.__doc__} \n"
            else:
                output_py_file += f"\n    {obj.__doc__} \n"

    output_py_file += f"""    \"\"\"
    # hydra args:
    _target_: str = "{obj.__module__}.{obj.__name__}"
    _recursive_: bool = True
    _convert_: Any = None
    _partial_: bool = False

    """

    # append keywords, types and default values to the dataclass
    for i, (key, value) in enumerate(inspect.signature(obj).parameters.items()):
        try:
            if value.kind == inspect.Parameter.VAR_KEYWORD:
                continue
        except Exception:  # pylint: disable=broad-except
            pass
        type_str = get_types_from_signature(value)
        default_value, type_str = validate_default_value(value.default, type_str)

        if i == 0:
            output_py_file += "# dataclass args:\n    "

        output_py_file += f"{key}: {type_str} = {default_value}\n    "
    output_py_file += "\n"

    return output_py_file


def main(import_str: str, add_orig_docstring: bool = True):
    """Convert a module or class to a file containing hydra compatible dataclasses.

    Args:
        import_str (str): The import string of the module or class to be converted, for example "torch.utils.data"
            or "torch.utils.data.Dataset".
        add_orig_docstring (bool): If True, the docstring of the original object will be added to the dataclass.
    """
    target_module = evaluate_import_str(import_str)

    inspect_type = None
    # check for all classes in the target_module and create a dataclass for each:
    if inspect.ismodule(target_module):
        inspect_type = "module"
        output_py_file = f"""\"\"\"Hydra dataclasses for the module :py:mod:`{target_module.__name__}`.

This module was automatically generated by the script :py:mod:`mdml_tools.scripts.convert2hydra`.
\"\"\"
"""

    elif inspect.isclass(target_module):
        inspect_type = "class"
        output_py_file = f"""\"\"\"Hydra dataclass for :py:class:`{target_module.__module__}.{target_module.__name__}` .

This module was automatically generated by the script :py:mod:`mdml_tools.scripts.convert2hydra`.
\"\"\"

"""
    elif inspect.isfunction(target_module):
        inspect_type = "function"
        output_py_file = (
            """\"\"\"Hydra dataclass for :py:function: """
            f""""`{target_module.__module__}.{target_module.__name__}` .

This module was automatically generated by the script :py:mod:`mdml_tools.scripts.convert2hydra`.
\"\"\"

"""
        )
    else:
        raise ValueError(f"target_module is of type {type(target_module)} which is not supported.")

    output_py_file += """from dataclasses import dataclass
from typing import Any, Union
from enum import Enum

"""

    if inspect_type == "module":
        for name, obj in inspect.getmembers(target_module):
            # check if the mdule has an __all__ attribute and only create dataclasses for the objects in __all__:
            if hasattr(target_module, "__all__"):
                if name not in target_module.__all__:
                    continue
            if name.startswith("_"):
                continue

            if inspect.isclass(obj) or inspect.isfunction(obj):
                sphinx_link_str = "class" if inspect.isclass(obj) else "function"
                output_py_file = write_dataclass_to_file(output_py_file, name, obj, sphinx_link_str, add_orig_docstring)

    elif inspect_type == "class":
        output_py_file = write_dataclass_to_file(
            output_py_file, target_module.__name__, target_module, "class", add_orig_docstring
        )

    elif inspect_type == "function":
        output_py_file = write_dataclass_to_file(
            output_py_file, target_module.__name__, target_module, "function", add_orig_docstring
        )

    # check that there is exactly one empty line at the end of the file and not more:
    while output_py_file[-1] == "\n" and output_py_file[-2] == "\n":
        output_py_file = output_py_file[:-1]

    # check that the output file is black formatted:
    try:
        output_py_file = black.format_str(output_py_file, mode=black.FileMode())
    except Exception:
        print(output_py_file)
        raise

    print(output_py_file)
    # write the output file:
    with open(args.output_file, "w") as out_file_stream:
        out_file_stream.write(output_py_file)

    # check that the output file is valid python code:
    try:
        ast.parse(output_py_file)
    except SyntaxError as err:
        raise SyntaxError(f"SyntaxError in {args.output_file}") from err


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "--no-original-docstring", action="store_false", default=True, help="Add the original docstring."
    )
    parser.add_argument(
        "import_str",
        type=str,
        help="String containing the module to import. For example, 'mdml_tools.utils.read_scalars_from_tensorboard'.",
    )
    parser.add_argument(
        "output_file",
        type=str,
        help="Path to the output file. For example, " "'mdml_tools/hydra_models/read_scalars_from_tensorboard.py'.",
    )
    args = parser.parse_args()
    main(args.import_str, args.no_original_docstring)
