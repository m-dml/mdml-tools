"""This script converts all necessary torch, lightning etc.

classes to hydra models. The output needs manual adjustment! So running the tests after this script is a good idea. The
same goes for pre-commit and pylint.
"""

import subprocess
from argparse import ArgumentParser
from pathlib import Path


def main(args):
    module_to_convert = {
        "torch.nn.modules.activation": "activations.py",
        "torch.distributions": "distributions.py",
        "torch.nn.modules.loss": "loss.py",
        "torch.optim": "optimizer.py",
        "torch.utils.data": "dataloading.py",
        "torch.optim.lr_scheduler": "scheduler.py",
        "torchmetrics": "metrics.py",
        "pytorch_lightning.callbacks": "lightning_callbacks.py",
        "pytorch_lightning.loggers": "lightning_loggers.py",
        "pytorch_lightning.profilers": "lightning_profilers.py",
        "pytorch_lightning.strategies": "lightning_strategies.py",
        "pytorch_lightning.trainer": "lightning_trainer.py",
    }

    output_dir = args.output_dir

    for module, file_name in module_to_convert.items():
        output_file = Path(output_dir) / file_name

        subprocess.run(
            [
                "python",
                "-m",
                "mdml_tools.scripts.convert2hydra",
                module,
                str(output_file.absolute()),
            ],
            check=True,
        )


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "--output_dir",
        type=Path,
        default=Path(__file__).parent.parent / "hydra_models",
        help="The directory where the hydra models will be saved.",
    )
    parsed_args = parser.parse_args()
    main(parsed_args)
