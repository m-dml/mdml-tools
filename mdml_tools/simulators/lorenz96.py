from typing import Literal, Union

import torch
from torch import nn

from mdml_tools.simulators.base import BaseSimulator


def l96_tendencies_x(
    x: torch.Tensor,
    forcing: Union[float, torch.Tensor, nn.Parameter],
) -> torch.Tensor:
    """Calculate Lorenz'96 tendencies of slow variable X.

        dx[k]/dt = -x[k-2] x[k-1] + x[k-1] x[k+1] - x[k] + F

    Args:
        x (torch.Tensor): x state variable [..., K]
        forcing (Union[float, torch.Tensor, nn.Parameter]): external forcing parameter

    Returns (torch.Tensor):
        tendencies of x variable [..., K]
    """
    k = x.size(-1)
    batch_size = x.size(0)
    x_reshaped = x.view(batch_size, k)

    xdot = (
        torch.roll(x_reshaped, shifts=1, dims=-1)
        * (torch.roll(x_reshaped, shifts=-1, dims=-1) - torch.roll(x_reshaped, shifts=2, dims=-1))
        - x_reshaped
        + forcing
    )
    xdot = xdot.view(batch_size, k)
    while len(xdot.shape) < len(x.shape):
        xdot = xdot.unsqueeze(1)

    return xdot


def l96_tendencies_xy(
    x: torch.Tensor,
    y: torch.Tensor,
    forcing: Union[float, torch.Tensor, nn.Parameter],
    b: Union[float, torch.Tensor, nn.Parameter],
    c: Union[float, torch.Tensor, nn.Parameter],
    h: Union[float, torch.Tensor, nn.Parameter],
) -> (torch.Tensor, torch.Tensor):
    """Calculate Lorenz'96 tendencies of fast variable Y.

        dx[k]/dt = -x[k-2] x[k-1] + x[k-1] x[k+1] - x[k] + F - hc/b sum(y[k, j])_j
        dy[j]/dt = -b c y[j+1] (y[j+2] - y[j-1]) - cy[j] + hc/b * x[k]

    The problem for y is, that at the boundaries we need to look at the next/previous point in x, not periodically
    go to the other side of the grid.

    Args:
        x (torch.Tensor): x state variable [BATCH, K]
        y (torch.Tensor): x state variable [BATCH, K, J]
        forcing (Union[float, torch.Tensor, nn.Parameter]): external forcing parameter
        b (Union[float, torch.Tensor, nn.Parameter]): ratio of amplitudes of fast and slow variables
        c (Union[float, torch.Tensor, nn.Parameter]): time-scale ratio of fast and slow variables
        h (Union[float, torch.Tensor, nn.Parameter]): coupling parameter

    Returns (torch.Tensor):
        tendencies of x and y variables [BATCH, K], [BATCH, K, J]
    """
    if x.size(0) == y.size(0):
        batch_size = x.size(0)
    else:
        raise ValueError("x and y must have the same batch size")

    k = x.size(-1)
    j = y.size(-1)

    x_reshaped = x.view(batch_size, k)
    y_reshaped = y.view(batch_size, k, j)

    hcb = (h * c) / b
    xdot = l96_tendencies_x(x_reshaped, forcing)
    xdot -= hcb * y_reshaped.sum(dim=-1)

    xdot = xdot.view(batch_size, k)
    while len(xdot.shape) < len(x.shape):
        xdot = xdot.unsqueeze(1)

    # we need to prepend each y-grid with the y-value from the previous x-grid and append the y-value from the next
    # x-grid, so that when we roll, we get the correct values at the boundaries:

    assert j > 2, "j must be greater than 2, so that no problems occur at the boundaries when rolling"

    y_flat = y_reshaped.view(batch_size, -1)
    ydot = (
        -b
        * c
        * y_flat.roll(shifts=-1, dims=-1).view(batch_size, k, j)
        * (
            y_flat.roll(shifts=-2, dims=-1).view(batch_size, k, j)
            - y_flat.roll(shifts=1, dims=-1).view(batch_size, k, j)
        )
        - c * y_reshaped
        + hcb * x_reshaped.unsqueeze(-1)
    )

    ydot = ydot.view(batch_size, k, j)
    while len(ydot.shape) < len(y.shape):
        ydot = ydot.unsqueeze(1)

    return xdot, ydot


class L96SimulatorOneLevel(BaseSimulator):
    """One level Lorenz'96 simulator.

        dx[k]/dt = (x[k+1] - x[k-2]) * x[k-1] - x[k] + F

    Args:
        forcing (Union[float, torch.Tensor, nn.Parameter]): external forcing parameter
        method (str): name of solving method from torchdiffeq
        options (dict): solver parameters

    Example:
        .. code-block:: python

            from mdml_tools.simulators.lorenz96 import L96SimulatorOneLevel
            k, f = 40, 8.0
            n_steps, spin_up_steps, dt = 500, 300, 0.01
            model = L96SimulatorOneLevel(f)
            x_init = f * (0.5 + torch.randn(torch.Size((1, 1, k,)), device="cpu") * 1.0)
            t = torch.arange(0, dt * (n_steps + spin_up_steps), dt)
            x = model.integrate(t, x_init)[:, spin_up_steps:, :]
    """

    def __init__(self, forcing: Union[float, torch.Tensor, nn.Parameter], method: str = "rk4", **kwargs):
        super().__init__(method=method, **kwargs)
        self.forcing = forcing

    def forward(self, time: float, state: torch.Tensor):
        return l96_tendencies_x(state, self.forcing)


class L96SimulatorTwoLevel(BaseSimulator):
    """Two levels Lorenz'96 simulator.

        dx[k]/dt = (x[k+1] - x[k-2]) * x[k-1] - x[k] + F - h c y[k].mean()
        dy[k, j]/dt = c * (b * y[k, j-1] * (y[k, j-1] - y[k, j+2]) - y[k, j] + h/J x[k])

    Args:
        forcing (Union[float, torch.Tensor, nn.Parameter]): external forcing parameter
        b (Union[float, torch.Tensor, nn.Parameter]): coupling parameter
        c (Union[float, torch.Tensor, nn.Parameter]): coupling parameter
        h (Union[float, torch.Tensor, nn.Parameter]): coupling parameter
        method (str): name of solving method from torchdiffeq
        options (dict): solver parameters

    Example:
        .. code-block:: python

            from mdml_tools.simulators.lorenz96 import L96SimulatorTwoLevel
            k, j, f = 40, 10, 8.0
            n_steps, spin_up_steps, dt = 500, 300, 0.01
            model = L96SimulatorTwoLevel(f)
            x_init = f * (0.5 + torch.randn(torch.Size((1, 1, k,)), device="cpu") * 1.0) / torch.tensor([j, 50]).max()
            y_init = f * (0.5 + torch.randn(torch.Size((1, 1, k, j)), device="cpu") * 1.0) / torch.tensor([j, 50]).max()
            t = torch.arange(0, dt * (n_steps + spin_up_steps), dt)
            x, y = model.integrate(t, (x_init, y_init))
            x = x[:, spin_up_steps:, ...]
            y = y[:, spin_up_steps:, ...]
    """

    def __init__(
        self,
        forcing: Union[float, torch.Tensor, nn.Parameter] = 10,
        b: Union[float, torch.Tensor, nn.Parameter] = 10,
        c: Union[float, torch.Tensor, nn.Parameter] = 1,
        h: Union[float, torch.Tensor, nn.Parameter] = 10,
        method: str = "rk4",
        **kwargs,
    ):
        super().__init__(method=method, **kwargs)
        self.forcing = forcing
        self.b = b
        self.c = c
        self.h = h

    def forward(self, time: float, state: (torch.Tensor, torch.Tensor)) -> tuple[torch.Tensor, torch.Tensor]:
        """Compute the tendencies of the state.

        Args:
            time (float): current time
            state (torch.Tensor): current state

        Returns:
            tuple[torch.Tensor, torch.Tensor]: tendencies of the state. The first element is the tendencies of the x
                variable, the second element is the tendencies of the y variable.
        """
        x, y = state
        xdot, ydot = l96_tendencies_xy(x, y, self.forcing, self.b, self.c, self.h)
        return xdot, ydot


# this function name is on purpose capitalized to be consistent with the other simulators and behaves like a class:
def L96Simulator(
    simulator_type: Literal["one_level", "two_level"] = "one_level", **kwargs
) -> Union[L96SimulatorOneLevel, L96SimulatorTwoLevel]:
    """Wrapper function for the two simulators that uses a literal string to select the simulator.

    Args:
        simulator_type (Literal["one_level", "two_level"], optional): Type of the simulator. Defaults to "one_level".

    Returns:
        Union[L96SimulatorOneLevel, L96SimulatorTwoLevel]: Instance of the selected simulator.
    """
    if simulator_type == "one_level":
        return L96SimulatorOneLevel(**kwargs)
    if simulator_type == "two_level":
        return L96SimulatorTwoLevel(**kwargs)

    raise ValueError(f"Simulator type {simulator_type} not supported.")
