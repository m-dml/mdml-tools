from abc import abstractmethod
from typing import TypeVar, Union

import torch
from torch import nn

try:
    from torchdiffeq import odeint, odeint_adjoint
except ImportError as err:
    raise ImportError(
        "torchdiffeq is not installed but needed when using the simulators. "
        "Pleas install e.g. with `pip install torchdiffeq`"
    ) from err


# declare a recursive for a tuple and a list with as many tensors as needed:
TupleOfTensors = TypeVar("TupleOfTensors", bound=tuple[torch.Tensor, ...])
ListOfTensors = TypeVar("ListOfTensors", bound=list[torch.Tensor, ...])


class BaseSimulator(nn.Module):
    """Fully-Differentiable Abstract Simulator Use differentiable solvers from torchdiffeq. see
    https://github.com/rtqichen/torchdiffeq.

    Args:
        use_adjoint (bool): allow solving with as many steps as necessary due to O(1) memory usage
        method (str): name of solving method from torchdiffeq
        options (dict): solver parameters
    """

    def __init__(
        self,
        *,
        use_adjoint: bool = True,
        method: str,
        options: dict = None,
        **kwargs,
    ):
        super().__init__()
        self.use_adjoint = use_adjoint
        self.method = method
        self.options = options

        # Due to inheritance of the hydra models these kwargs might be passed to the base class by hydra:
        _allowed_kwargs = ["b", "c", "h", "forcing"]
        for kwarg in kwargs:
            # check if kwargs only contains allowed keys to prevent errors further downstream.
            # We can safely ignore the allowed kwargs here:
            if kwarg not in _allowed_kwargs:
                raise ValueError(f"Unknown keyword argument provided to BaseSimulator: {kwarg}")

    @abstractmethod
    def forward(
        self,
        time: Union[float, torch.Tensor],
        state: Union[torch.Tensor, TupleOfTensors],
    ) -> Union[torch.Tensor, TupleOfTensors]:
        """Implement the method to calculate tendencies of the model
            This method is going to be warped into chosen solver
        Args:
            time (float): time variable. It is used by torchdiffeq.odeint while solving.
            state (Union[torch.Tensor, TupleOfTensors]): initial conditions tensor or a tuple of tensors

        Returns (Union[torch.Tensor, TupleOfTensors]):
            tendencies for each variable provided as input to the method
        """
        raise NotImplementedError

    def integrate(
        self,
        time: torch.Tensor,
        state: Union[torch.Tensor, TupleOfTensors],
    ) -> Union[torch.Tensor, ListOfTensors]:
        """Solve the system using chosen method from torchdiffeq
        Args:
            time (torch.Tensor): tensor with timepoints [Time]
            state (Union[torch.Tensor, TupleOfTensors]): initial conditions tensor with shape [Batch, ...] or list of
                tensors

        Returns (Union[torch.Tensor, ListOfTensors]):
            simulated variables. returns several simulations if batch dimension higher than one [Batch, Time, ...]
        """
        solve = odeint_adjoint if self.use_adjoint else odeint
        rollout = solve(func=self, y0=state, t=time, method=self.method, options=self.options)
        if isinstance(rollout, tuple):
            rollout = list(rollout)
        elif isinstance(rollout, torch.Tensor):
            rollout = [rollout]

        for i, variable in enumerate(rollout):
            rollout[i] = variable.swapdims(0, 1)
            batch_size = rollout[i].size(0)
            rollout[i] = rollout[i].squeeze()
            if batch_size == 1:
                rollout[i] = rollout[i].unsqueeze(0)

        if len(rollout) == 1:
            rollout = rollout.pop()
        return rollout
