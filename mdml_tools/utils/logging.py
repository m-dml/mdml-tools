"""Module containing all kind of logging utilities.

This can be from logging to the console, to logging to a tensorboard
"""

import inspect
import logging
from logging import StreamHandler
from typing import get_args, Optional, Union

import pytorch_lightning
import rich.syntax
import rich.tree
from omegaconf import DictConfig, OmegaConf
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.utilities import rank_zero_only as torch_rank_zero_only

TB_LOGGER = None
LOG_LEVEL = "INFO"


def set_tb_logger(tb_logger: TensorBoardLogger):
    """Sets the global TB_LOGGER variable. This is used by the get_logger() function to add a TensorBoard logging
    handler which prints the console output to TensorBoard.

    Args:
        tb_logger (TensorBoardLogger): Pytorch Lightning TensorBoard logger .
    """
    global TB_LOGGER  # pylint: disable=W0603
    TB_LOGGER = tb_logger


def set_log_levels(level="INFO"):
    """Sets the log level for all loggers.

    Args:
        level (str, optional): Log level. Defaults to "INFO".
    """
    global LOG_LEVEL  # pylint: disable=W0603
    LOG_LEVEL = level.strip().upper()
    loggers = [logging.getLogger(name) for name in logging.root.manager.loggerDict]
    for logger in loggers:
        logger.setLevel(logging.getLevelName("INFO"))


@torch_rank_zero_only
def print_config(config: DictConfig, resolve: bool = True) -> None:
    """Prints content of DictConfig using Rich library and its tree structure.

    Args:
        config (DictConfig): Configuration composed by Hydra.
        resolve (bool, optional): Whether to resolve reference fields of DictConfig.
    """
    style = "dim"
    tree = rich.tree.Tree(":gear: CONFIG", style=style, guide_style=style)

    for field in config.keys():
        branch = tree.add(field, style=style, guide_style=style)

        config_section = config.get(field)
        branch_content = str(config_section)
        if isinstance(config_section, DictConfig):
            branch_content = OmegaConf.to_yaml(config_section, resolve=resolve)

        branch.add(rich.syntax.Syntax(branch_content, "yaml"))

    rich.print(tree)


def get_logger(name: str = __name__, level: Union[str, None] = None) -> logging.Logger:
    """Initializes multi-GPU-friendly python logger.

    Args:
        name (str, optional): Logger name. Defaults to __name__.
        level (str, optional): Log level. If not set (None) uses global log level. Defaults to None.
    """
    new_logger = logging.getLogger(name)
    if TB_LOGGER is not None:
        new_logger.addHandler(TBLoggingHandler(TB_LOGGER))
    if not level:
        level_obj = logging.getLevelName(LOG_LEVEL.strip().upper())
    else:
        level_obj = logging.getLevelName(level.strip().upper())
    new_logger.setLevel(level_obj)

    return new_logger


@torch_rank_zero_only
def get_hparams_from_hydra_config(
    config: DictConfig, model: Union[pytorch_lightning.LightningModule, None] = None
) -> dict:
    """This method returns a dictionary of hyperparameter from a Hydra configuration. This can be used to log the
    hyperparameter to TensorBoard.

    Args:
        config (DictConfig): Hydra configuration.
        model (pytorch_lightning.LightningModule, optional): Pytorch Lightning model. Defaults to None. If provided,
            the model parameter counts will be added to the hyperparameter dictionary.
    """

    hparams = {}

    for field in config.keys():
        config_section = config.get(field)
        _types = Union[dict, DictConfig]
        if isinstance(config_section, get_args(_types)):
            hparams.update(config_section)
        else:
            hparams[field] = config_section

    # save number of model parameters
    if model is not None:
        hparams["params_total"] = sum(p.numel() for p in model.parameters())
        hparams["params_trainable"] = sum(p.numel() for p in model.parameters() if p.requires_grad)
        hparams["params_not_trainable"] = sum(p.numel() for p in model.parameters() if not p.requires_grad)

    return hparams


def dump_all_locals(logger: Optional[logging.Logger] = None, level: str = "ERROR"):
    """Dumps all local variables to the logger. Useful for debugging.

    Args:
        logger (logging.Logger, optional): Logger to use. Defaults to None.
        level (str, optional): Log level at which the output will be logged. Defaults to "ERROR".
    """
    if logger is None:
        logger = get_logger(__name__)

    frame = inspect.currentframe()
    frame = frame.f_back

    log_level = logging.getLevelName(level.strip().upper())
    logger.log(log_level, "Dumping all local variables:")

    for key, value in frame.f_locals.items():
        logger.log(log_level, f"{key} = {value}")


class TBLoggingHandler(StreamHandler):
    """A stream handler which can be added to a standard python logger to log to TensorBoard.

    Args:
        logger (TensorBoardLogger): Pytorch Lightning TensorBoard logger.

    Examples:
        .. code-block:: python

            from mdml_tools.utils.logging import get_logger
            from pytorch_lightning.loggers import TensorBoardLogger
            tb_logger = TensorBoardLogger("tb_logs")
            set_tb_logger(tb_logger)
            logger = getLogger(__name__)
            logger.info("Hello TensorBoard!")
    """

    def __init__(self, logger: TensorBoardLogger, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.writer = logger.experiment
        self._pseudo_step = 0

    def emit(self, record):
        """Emits a log record to TensorBoard.

        Args:
            record (logging.LogRecord): Log record.
        """
        msg = self.format(record)
        self.writer.add_text("log", msg, global_step=self._pseudo_step)
        self._pseudo_step += 1
