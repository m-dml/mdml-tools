"""Module containing a fully differentiable min-max scaler.

It does work the same way as the numpy min-max scaler, but it is fully differentiable and can be used in pytorch models.
"""

from abc import abstractmethod
from typing import Collection, Iterable, Optional, Union

import torch

from mdml_tools.utils.logging import get_logger


class _BaseScaler:
    """Base class for all scalers.

    Args:
        data_loader (Iterable, Collection): Iterable that yields batches of data. If provided, data_min and data_max are
            calculated from the data. If data_min and data_max are provided, the data_loader is ignored.
        data_loader_samples (float): Number of samples to use from the data_loader to calculate min and max. If less
            than 1, it is interpreted as a relative number of samples. This only works if the data_loader has a __len__
            method (So it does not work with iterative dataloaders). If None, all samples are used.
        data_loader_dim (int): Dimension of the data_loader that contains the data. If None, all data is used. If your
            dataloader returns a tuple of 2 values (x, y) but you only want to normalize x, then this should be set to
            0. This variable is ignored when the data_loader is ignored. Has to provide data in the shape of
            (batch-size, ...).
    """

    def __init__(
        self,
        data_loader: Optional[Union[Iterable, Collection]] = None,
        data_loader_samples: Optional[float] = None,
        data_loader_dim: int = 0,
    ):
        super().__init__()
        self.console_logger = get_logger(__name__)
        self.data_loader = data_loader
        self.data_loader_samples = data_loader_samples
        self.data_loader_dim = data_loader_dim

    def forward(self, data):
        return self.transform(data)

    def reformat_data_loader_samples(self, data_loader_samples: float) -> int:
        """Checks if the data_loader_samples are absolute or relative and converts them to absolute.

        Args:
            data_loader_samples (float): Absolute or relative number of samples to use from the data_loader.

        Returns:
            int: Absolute number of samples to use from the data_loader to calculate min and max.
        """
        if data_loader_samples is None:
            if hasattr(self.data_loader, "__len__"):
                data_loader_samples = len(self.data_loader)
            else:
                raise ValueError(
                    "data_loader_samples is None, but data_loader does not have a __len__ method. "
                    "Please provide an absolute number of samples."
                )
        elif data_loader_samples <= 1:
            try:
                len(self.data_loader)
            except TypeError as exc:
                raise ValueError(
                    "data_loader_samples is relative, but data_loader does not have a __len__ method. "
                    "Please provide an absolute number of samples."
                ) from exc
            batch = next(iter(self.data_loader))
            batch_size = batch[self.data_loader_dim].shape[0]

            data_loader_samples = int(data_loader_samples * len(self.data_loader) * batch_size)
            self.console_logger.info(
                f"Converted relative data_loader_samples to {data_loader_samples} absolute " "samples to use."
            )
        return data_loader_samples

    @abstractmethod
    def transform(self, data: torch.Tensor):
        raise NotImplementedError

    @abstractmethod
    def inverse_transform(self, data: torch.Tensor):
        raise NotImplementedError

    @abstractmethod
    def fit(self):
        raise NotImplementedError


class MinMaxScaler(_BaseScaler):
    """Classic min-max scaler, but completely in torch (so differentiable).

    Args:
        data_min (float): Minimum value of the data (before transformation).
        data_max (float): Maximum value of the data (before transformation).
        clip_min (float): Minimum value of the transformed data. Values below this will be clipped to this value. Using
            this means that inverted data will not be the same as original data!
        clip_max (float): Maximum value of the transformed data. Values above this will be clipped to this value. Using
            this means that inverted data will not be the same as original data!
        target_min (int): Minimum value of the transformed data.
        target_max (int): Maximum value of the transformed data.
    """

    def __init__(
        self,
        data_min: float = None,
        data_max: float = None,
        clip_min: float = None,
        clip_max: float = None,
        target_min: int = 0,
        target_max: int = 1,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.data_min = data_min
        self.data_max = data_max
        self.clip_min = clip_min
        self.clip_max = clip_max
        self.target_min = target_min
        self.target_max = target_max
        self.check_inputs()

    def check_inputs(self):
        """Checks the combination of inputs and sets defaults if necessary.

        If only a data-loader (or other iterable) is provided, the data-min and data-max are calculated from the data.
        If data-min and data-max are provided, the data-loader is ignored.
        """
        if all(x is None for x in [self.data_loader, self.data_min, self.data_max]):
            raise ValueError("Either data_loader or data_min and data_max must be provided.")
        if all(x is not None for x in [self.data_loader, self.data_min, self.data_max]):
            self.console_logger.warning(
                "Both data_loader and data_min and data_max are provided. Data_loader will be ignored."
            )
            return

        if self.data_min is None and self.data_max is not None:
            raise ValueError("data_min must be provided if data_max is provided.")
        if self.data_min is not None and self.data_max is None:
            raise ValueError("data_max must be provided if data_min is provided.")

        if self.data_loader is not None:
            self.console_logger.info("Fitting MinMaxScaler to data_loader.")
            self.fit()

    def fit(self):
        """Uses the data_loader to find the min and max of the data provided. If data_loader_dim is provided, only the
        data in that dimension is used.

        Sets:
            self.data_min (float): Minimum value of the data (before transformation).
            self.data_max (float): Maximum value of the data (before transformation).
        """

        self.data_loader_samples = self.reformat_data_loader_samples(self.data_loader_samples)

        for i, batch in enumerate(self.data_loader):
            batch = batch[self.data_loader_dim]
            if self.data_min is None:
                self.data_min = batch.min().item()
                self.data_max = batch.max().item()
            else:
                self.data_min = min(self.data_min, batch.min().item())
                self.data_max = max(self.data_max, batch.max().item())

            if i > self.data_loader_samples:
                break

        self.console_logger.info(f"Inferred data min: {self.data_min}; data max: {self.data_max}")

    def transform(self, data: torch.Tensor) -> torch.Tensor:
        """Transforms the data to be between target_min and target_max.

        Args:
            data (torch.Tensor): Data to be transformed.

        Returns:
            torch.Tensor: Transformed data.
        """
        transformed = (
            ((data - self.data_min) / (self.data_max - self.data_min)) * (self.target_max - self.target_min)
        ) + self.target_min
        if self.clip_min is not None:
            transformed = torch.max(transformed, torch.tensor(self.clip_min))
        if self.clip_max is not None:
            transformed = torch.min(transformed, torch.tensor(self.clip_max))
        return transformed

    def inverse_transform(self, data: torch.Tensor) -> torch.Tensor:
        """Undo the transformation, so that the data is between data_min and data_max. If clip_min or clip_max are used,
        the inverse transformation will not be the same as the original data.

        Args:
            data (torch.Tensor): Transformed data to be re-transformed to its original range.

        Returns:
            torch.Tensor: Data in original range.
        """
        return (
            ((data - self.target_min) / (self.target_max - self.target_min)) * (self.data_max - self.data_min)
        ) + self.data_min


class NormalScaler(_BaseScaler):
    """Scales data to have mean 0 and std 1 (unless another mean and std are given).

    Args:
        data_mean (float): Mean of the data (before transformation).
        data_std (float): Standard deviation of the data (before transformation).
        target_mean (float): Mean of the transformed data.
        target_std (float): Standard deviation of the transformed data.
        max_memory_usage (float): Maximum memory usage in bytes. If the data_loader is provided, the data is split into
            chunks of multiple batches and the mean and std are calculated for each chunk. The chunks are then combined.
    """

    def __init__(
        self,
        data_mean: Optional[float] = None,
        data_std: Optional[float] = None,
        target_mean: float = 0,
        target_std: float = 1,
        max_memory_usage: float = 1e9,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.data_mean = data_mean
        self.data_std = data_std
        self.target_mean = target_mean
        self.target_std = target_std
        self.max_memory_usage = max_memory_usage
        self.check_inputs()

    def check_inputs(self):
        """Checks the combination of inputs and sets defaults if necessary.

        If only a data-loader (or other iterable) is provided, the data-mean and data-std are calculated from the data.
        If data-mean and data-std are provided, the data-loader is ignored.
        """
        if all(x is None for x in [self.data_loader, self.data_mean, self.data_std]):
            raise ValueError("Either data_loader or data_mean and data_std must be provided.")
        if all(x is not None for x in [self.data_loader, self.data_mean, self.data_std]):
            self.console_logger.warning(
                "Both data_loader and data_mean and data_std are provided. Data_loader will be ignored."
            )
            return

        if self.data_mean is None and self.data_std is not None:
            raise ValueError("data_mean must be provided if data_std is provided.")
        if self.data_mean is not None and self.data_std is None:
            raise ValueError("data_std must be provided if data_mean is provided.")

        if self.data_loader is not None:
            self.console_logger.info("Fitting NormalScaler to data_loader.")
            self.fit()

    def fit(self):
        """Uses the data_loader to find the mean and std of the data provided. If data_loader_dim is provided, only the
        data in that dimension is used.

        For calculating the mean and std, as much of the data as possible is kept in memory. If the data is too large to
        fit in memory, the data is split into chunks and the mean and std are calculated for each chunk. The mean and
        std of the chunks are then combined to find the mean and std of the whole dataset.

        Sets:
            self.data_mean (float): Mean value of the data (before transformation).
            self.data_std (float): Standard deviation of the data (before transformation).
        """

        self.data_loader_samples = self.reformat_data_loader_samples(self.data_loader_samples)

        self.data_mean = 0
        self.data_std = 0

        # get a test batch to check the shape
        test_batch = next(iter(self.data_loader))[self.data_loader_dim]
        test_batch_memory = test_batch.nelement() * test_batch.element_size()

        # create an empty tensor to store the data that fits in max_memory_usage
        max_empty_size = int(self.max_memory_usage // test_batch_memory)
        batches = torch.empty(
            (max_empty_size, len(test_batch.flatten())), dtype=test_batch.dtype, device=test_batch.device
        )
        batches_size = batches.nelement() * batches.element_size()

        assert max_empty_size > 0, "max_memory_usage is too small to fit a single batch."
        assert batches_size <= self.max_memory_usage, "max_memory_usage is too small to fit a single batch."

        chunk_means = []
        chunk_stds = []

        i = 0
        for i, batch in enumerate(self.data_loader):
            batch = batch[self.data_loader_dim]
            batches[i % max_empty_size] = batch.flatten()

            if i > 0 and i % max_empty_size == 0:
                chunk_means.append(torch.mean(batches))
                chunk_stds.append(torch.std(batches))

            if i > self.data_loader_samples:
                break

        batches = batches[: i % max_empty_size]

        if len(batches) > 0:
            chunk_means.append(torch.mean(batches))
            chunk_stds.append(torch.std(batches))

        self.data_mean = torch.mean(torch.stack(chunk_means)).item()
        self.data_std = torch.mean(torch.stack(chunk_stds)).item()

    def transform(self, data: torch.Tensor) -> torch.Tensor:
        """Transforms the data to have mean target_mean and std target_std.

        Args:
            data (torch.Tensor): Data to be transformed.

        Returns:
            torch.Tensor: Transformed data.
        """
        return ((data - self.data_mean) / self.data_std) * self.target_std + self.target_mean

    def inverse_transform(self, data: torch.Tensor) -> torch.Tensor:
        """Undo the transformation, so that the data has mean data_mean and std data_std.

        Args:
            data (torch.Tensor): Transformed data to be re-transformed to its original range.

        Returns:
            torch.Tensor: Data in original range.
        """
        return ((data - self.target_mean) / self.target_std) * self.data_std + self.data_mean
