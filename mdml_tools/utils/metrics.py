"""Module for custom metrics."""

from typing import Callable

import torch
from torch import nn


def linear_activation(x: torch.Tensor) -> torch.Tensor:
    """Linear activation function."""
    return x


def _get_reduction_function(reduction: str) -> Callable:
    reduction = reduction.lower().strip()
    if reduction == "mean":
        reduction = torch.mean
    elif reduction == "sum":
        reduction = torch.sum
    elif reduction == "none" or reduction is None:
        reduction = linear_activation  # noqa: E731
    else:
        raise NotImplementedError(f"Reduction <{reduction}> is not implemented.")
    return reduction


class RootEuclidianDistance(nn.Module):
    """Calculates the root euclidian distance between the ensemble inputs and the targets.

    The metric is from the paper "Pareto GAN: Extending the Representational Power of GANs to Heavy-Tailed
    Distributions" eq. 12. It calculates the L2 norm and takes the nth root of it.

    Unlike in the orignal L2 norm the mean is used, not the sum. This is to make the metric independent of the batch
    size and makes it more comparable to the MSE loss for example.

    Args:
        root (int): Which root to take of the L2 norm. Default is 2 (=square root).
    """

    def __init__(self, root=2):
        super().__init__()
        self.root = root

    def forward(self, inputs: torch.Tensor, targets: torch.Tensor, reduction: str = "mean") -> torch.Tensor:
        """Calculates the root euclidian distance between the ensemble inputs and the targets. The metric is from the
        paper "Pareto GAN: Extending the Representational Power of GANs to Heavy-Tailed Distributions" eq. 12.

        Args:
            inputs (torch.Tensor): Ensemble inputs of shape (ensemble_size, batch_size, *) or (batch_size, *).
            targets (torch.Tensor): Target of shape (batch_size, *).
            reduction (str): Reduction method for the loss. Default is "mean". If "none", the loss is not reduced and
                has an entry for each sample in the batch.

        Returns:
            torch.Tensor: Loss
        """
        reduction = _get_reduction_function(reduction)

        assert len(inputs.shape) == len(targets.shape), (
            f"Inputs and targets must have the same number of dimensions. Got shapes "
            f"inputs: <{inputs.shape}> and targets: <{targets.shape}>."
        )

        distance = reduction((inputs - targets).flatten(start_dim=1).pow(2).mean(dim=1) ** (1 / (2 * self.root)))
        return distance
