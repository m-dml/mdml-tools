"""Module containing custom hydra callbacks."""

import logging
from typing import Any

import git
from hydra.core.hydra_config import HydraConfig
from hydra.experimental.callback import Callback
from omegaconf import DictConfig


class LogGitHash(Callback):
    """Git hash logger hydra callback. Log git hash to a text file when a hydra-submitit job is started. The text file
    will be located in the hydra output directory.

    To use this callback, add the following to your hydra config.

    In your config folder add a directory structure like this:

    ::

        config
        └───hydra
            └───callbacks
                └───log_git_hash.yaml

    In the log_git_hash.yaml file add the following:

    .. code-block:: yaml
        :caption: log_git_hash.yaml

        # @package _global_
        hydra:
          callbacks:
            log_git_hash:
              _target_: "mdml_tools.utils.hydra_callbacks.LogGitHash"
              git_hash: null

    In the main config in your defaults list your have to reference the log_git_hash.yaml file like this:

    .. code-block:: yaml
        :caption: config.yaml

        defaults:
            - /hydra/callbacks:
                - log_git_hash
    """

    def __init__(self):
        self.git_hash = None
        self.console_logger = logging.getLogger(__name__)

    @staticmethod
    def get_git_hash(repo_destination: str):
        """Get git hash of the current commit.

        Args:
            repo_destination (str): path to the git repository. Parent directories will be searched.
        """
        repo = git.Repo(path=repo_destination, search_parent_directories=True)
        sha = repo.head.object.hexsha
        return sha

    @staticmethod
    def save_hash_to_file(git_hash: str, file_name="git_hash.txt"):
        """Save git hash to a text file.

        Args:
            git_hash (str): git hash string.
            file_name (str): name of the file to save the hash to.
        """
        with open(file_name, "w") as file:
            file.write(git_hash)

    def on_job_start(self, config: DictConfig, **kwargs: Any) -> None:
        """Log git hash to a text file when slurm submitit job is started. Attaches to the default hook.

        Args:
            config (DictConfig): hydra config.
            **kwargs (Any): additional arguments to catch any not used arguments submitted by submitit.
        """
        repo_destination = HydraConfig.get().runtime.cwd
        self.git_hash = self.get_git_hash(repo_destination)
        self.console_logger.info(f"Git hash: {self.git_hash}")
        self.save_hash_to_file(self.git_hash)
