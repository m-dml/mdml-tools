"""Module containing the utility function to read scalars from tensorboard logfile."""

from pathlib import Path
from typing import Union

import pandas as pd
from tensorboard.backend.event_processing import event_accumulator


def read_scalars_from_tensorboard(logfile: Union[str, Path]) -> dict[str, pd.DataFrame]:
    """Read all scalars from tensorboard file.

    Each scalar from the logfile is converted to pandas a DataFrame to make it easy to plot (see example below).

    Args:
        logfile (str): Path to the tensorboard log-file.

    Example:
        >>> from mdml_tools.utils import read_scalars_from_tensorboard
        >>> import matplotlib.pyplot as plt
        >>> scalars = read_scalars_from_tensorboard("path_to_your_tensorboard_file")
        >>> scalars["loss"].plot(x="step", y="value", label="loss")
        >>> plt.show()
    """
    event = event_accumulator.EventAccumulator(
        logfile,
        size_guidance={
            event_accumulator.SCALARS: 0,
        },
    )
    event.Reload()
    scalars = event.Tags()["scalars"]
    return {key: pd.DataFrame(event.Scalars(key)) for key in scalars}
