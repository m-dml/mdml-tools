"""This module contains functions for profiling code."""


from contextlib import contextmanager
from functools import wraps
from logging import Logger
from typing import Optional, Union

import torch
from pytorch_lightning.loggers import Logger as LightningLogger

call_step = 0


def measure_execution_time(
    console_logger: Optional[Union[Logger, str]] = None,
    experiment_log_func: Optional[Union[LightningLogger.log_metrics, str]] = None,
    bypass: bool = False,
    profile_memory_diff: bool = True,
    is_class_method: bool = False,
):
    """Decorator for measuring the execution time of a function.

    Args:
        console_logger (logging.Logger, optional): Logger to use. Defaults to None.
        experiment_log_func (function, optional): Function to log to an experiment. Defaults to None.
        bypass (bool, optional): If True, the function is not profiled. Defaults to False. This can be used to
            disable/enable profiling for a whole module easily.
        profile_memory_diff (bool, optional): If True, the difference in memory usage before and after the function is
            profiled. Defaults to True.
        is_class_method (bool, optional): If True, the log-functions are evaluated as class methods. Defaults to False.
            This can be used to call something like "self.log" in the decorated function. In this case you would just
            pass "self.log" as string to the experiment_log_func and set is_class_method to True.

    Warnings:
        1. At the moment only the TensorboardLogger is supported for logging to an experiment.
        2. When no class method is decorated, so that the gloabal step can be determined automatically, a call counter
            is used. This means that in that case the x-axis of the Tensorboard plot is not the global step, but the
            call step. This is not a problem, if the function is only called once per step, but if it is called multiple
            times, the x-axis will be wrong!

    Returns:
        function: Decorated function.

    Example:

        For logging a function to the Tensorboard, use the following:

        .. code-block:: python

            from mdml_tools.utils.profiling import measure_execution_time
            from pytorch_lightning.loggers import TensorBoardLogger

            tb_logger = TensorBoardLogger("logs/")

            @measure_execution_time(experiment_log_func=tb_logger.log_metrics)
            def train_loop(model, optimizer, loss, data):
                for batch_data in data:
                    ...

        For logging a class method to the Tensorboard, use the following:

        .. code-block:: python

            from mdml_tools.utils.profiling import measure_execution_time
            from pytorch_lightning.loggers import TensorBoardLogger

            class MyModel(LightningModule):
                def __init__(self):
                    ...
                    self.tb_logger = TensorBoardLogger("logs/")

                @measure_execution_time(experiment_log_func="self.tb_logger.log_metrics", is_class_method=True)
                def train_loop(self, optimizer, loss, data):
                    for batch_data in data:
                        ...
    """

    def decorate(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if bypass:
                return func(*args, **kwargs)

            _console_logger = console_logger
            _experiment_log_func = experiment_log_func
            if is_class_method:
                orig_cls = args[0]
                if console_logger is not None:
                    _console_logger = eval(console_logger, {"self": orig_cls})
                if experiment_log_func is not None:
                    _experiment_log_func = eval(experiment_log_func, {"self": orig_cls})

                    if not hasattr(orig_cls, "global_step"):
                        raise AttributeError(
                            "Class method is decorated with measure_execution_time and uses experiment_log_func, but "
                            "does not have a global_step attribute. Please add a <self.global_step> attribute to your "
                            "class."
                        )

            if not torch.cuda.is_available():
                if _console_logger:
                    _console_logger.warning("CUDA is not available. Profiling will not work!")
                else:
                    print("!!! CUDA is not available. Profiling will not work !!!")
                return func(*args, **kwargs)

            torch.cuda.synchronize()
            start = torch.cuda.Event(enable_timing=True)
            end = torch.cuda.Event(enable_timing=True)

            if profile_memory_diff:
                start_mem = torch.cuda.memory_allocated()
            start.record()
            result = func(*args, **kwargs)
            end.record()
            if profile_memory_diff:
                end_mem = torch.cuda.memory_allocated()

            torch.cuda.synchronize()

            elapsed_time = start.elapsed_time(end)
            if profile_memory_diff:
                memory_in_mb = (end_mem - start_mem) / 1024 / 1024

            if console_logger is not None:
                if profile_memory_diff:
                    _console_logger.debug(f"Profiling function {func.__name__}: {elapsed_time} ms | {memory_in_mb} MB")
                else:
                    _console_logger.debug(f"Profiling function {func.__name__}: {elapsed_time} ms")

            if _experiment_log_func is not None:
                if is_class_method:
                    _global_step = orig_cls.global_step
                else:
                    global call_step  # pylint: disable=global-statement
                    call_step += 1
                    _global_step = call_step
                _experiment_log_func(f"profile_time/{func.__name__} (ms)", elapsed_time, _global_step)
                if profile_memory_diff:
                    _experiment_log_func(f"profile_memory_diff/{func.__name__} (mb)", memory_in_mb, _global_step)

            return result

        return wrapper

    return decorate


@contextmanager
def measure_execution_time_context(
    context_name: str,
    console_logger: Optional[Union[Logger, str]] = None,
    experiment_log_func: Optional[Union[LightningLogger.log_metrics, str]] = None,
    bypass: bool = False,
    profile_memory_diff: bool = True,
    global_step: Optional[int] = None,
):
    """Context manager for profiling parts of code using CUDA.

    Args:
        context_name (str): Name of the context. This is necessary for reporting the results.
        console_logger (logging.Logger, optional): Logger to use. Defaults to None.
        experiment_log_func (function, optional): Function to log to an experiment. Defaults to None.
        bypass (bool, optional): If True, the function is not profiled. Defaults to False. This can be used to
            disable/enable profiling for a whole module easily.
        profile_memory_diff (bool, optional): If True, the difference in memory usage before and after the function is
            profiled. Defaults to True.
        global_step (int, optional): Global step to use for logging. Defaults to None. If None, the global step is
            set to a call counter. This means that in that case the x-axis of the Tensorboard plot is not the global
            step, but the call step. This is not a problem, if the function is only called once per step, but if it is
            called multiple times, the x-axis will be wrong!

    Warnings:
        1. At the moment only the TensorboardLogger is supported for logging to an experiment.

    Example:

        For logging teh overall execution and also each batch performance use e.g.:

        .. code-block:: python

            from mdml_tools.utils.profiling import measure_execution_time_context
            from pytorch_lightning.loggers import TensorBoardLogger

            tb_logger = TensorBoardLogger("logs/")


            with measure_execution_time_context("train_loop", experiment_log_func=tb_logger.log_metrics, global_step=1):
            for i, batch_data in enumerate(data):
                    with measure_execution_time_context(
                        "_train_loop_method",
                        experiment_log_func=tb_logger.log_metrics,
                        global_step=i):
                        optimizer.zero_grad()
                        ...
    """
    if bypass:
        yield
    else:
        if not torch.cuda.is_available():
            if console_logger:
                console_logger.warning("CUDA is not available. Profiling will not work!")
            else:
                print("!!! CUDA is not available. Profiling will not work !!!")
            yield
        else:
            # =========== init the profiling ===========
            torch.cuda.synchronize()
            start = torch.cuda.Event(enable_timing=True)
            end = torch.cuda.Event(enable_timing=True)

            if profile_memory_diff:
                start_mem = torch.cuda.memory_allocated()
            start.record()

            # =========== yield the context ==============
            yield

            # =========== finish the profiling ===========
            end.record()
            if profile_memory_diff:
                end_mem = torch.cuda.memory_allocated()

            torch.cuda.synchronize()

            elapsed_time = start.elapsed_time(end)
            if profile_memory_diff:
                memory_in_mb = (end_mem - start_mem) / 1024 / 1024

            # =========== log the results =================

            if console_logger is not None:
                if profile_memory_diff:
                    console_logger.debug(f"Profiling {context_name}: {elapsed_time} ms | {memory_in_mb} MB")
                else:
                    console_logger.debug(f"Profiling {context_name}: {elapsed_time} ms")

            if experiment_log_func is not None:
                if global_step is None:
                    global call_step  # pylint: disable=global-statement
                    call_step += 1
                    global_step = call_step

                experiment_log_func(f"profile_time/{context_name} (ms)", elapsed_time, global_step)
                if profile_memory_diff:
                    experiment_log_func(f"profile_memory_diff/{context_name} (mb)", memory_in_mb, global_step)
