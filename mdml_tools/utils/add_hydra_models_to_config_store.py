"""Module containing the utility function to add all models from mdml.hydra_models to the config store of ones
project."""


import glob
import importlib
import os
import re
from types import ModuleType
from typing import Optional

from hydra.core.config_store import ConfigStore

from mdml_tools import hydra_models

# defining groups to modules mapping (Keys must be the same as in CONFIG_GROUPS):
all_hydra_model_files = glob.glob(os.path.join(os.path.dirname(hydra_models.__file__), "*.py"))
# remove __init__.py and typing.py
all_hydra_model_files = [f for f in all_hydra_model_files if "__init__" not in f and "typing" not in f]
all_hydra_module_names = [os.path.splitext(os.path.basename(file))[0] for file in all_hydra_model_files]

CONFIG_GROUPS = {str(module): str(module) for module in all_hydra_module_names}
CONFIG_GROUPS_MODULES_MAPPING = {
    module: importlib.import_module(f"mdml_tools.hydra_models.{module}") for module in all_hydra_module_names
}

CONFIGS_THAT_ARE_LISTS = [
    "metric",
    "lightning_callback",
    "lightning_logger",
]

assert set(CONFIG_GROUPS.keys()) == set(
    CONFIG_GROUPS_MODULES_MAPPING.keys()
), "CONFIG_GROUPS and CONFIG_GROUPS_MODULES_MAPPING must have the same keys"


def _to_snake_case(name: str):
    """Helper functiion to convert CamelCase to snake_case.

    Args:
        name (str): CamelCase string
    """

    name = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", name)
    name = re.sub("__([A-Z])", r"_\1", name)
    name = re.sub("([a-z0-9])([A-Z])", r"\1_\2", name)
    return name.lower()


def _add_module_to_config_store_group(
    config_store: ConfigStore, module: ModuleType, group_name: str, group_is_list: bool
) -> ConfigStore:
    """Helper function to add all classes from a module to a group in the config store. The name is automatically
    converted to snake_case and the suffix "_base" is added to the name. If group_is_list is True, the name of the name
    of the class is added to the group name separated by a "/".

    Args:
        config_store (ConfigStore): ConfigStore instance
        module (ModuleType): Module containing the classes to add to the config store
        group_name (str): Name of the group to add the classes to
        group_is_list (bool): If True, the name of the class is added to the group name separated by a "/".
    """
    classes = [getattr(module, name) for name in dir(module) if isinstance(getattr(module, name), type)]
    for class_ in classes:
        if class_.__module__ == module.__name__:
            if group_is_list:
                config_store.store(
                    name=_to_snake_case(class_.__name__) + "_base",
                    node=class_,
                    group=group_name + f"/{_to_snake_case(class_.__name__)}",
                )
            else:
                config_store.store(name=_to_snake_case(class_.__name__) + "_base", node=class_, group=group_name)
    return config_store


def add_hydra_models_to_config_store(config_store: ConfigStore, rename_groups: Optional[dict] = None) -> ConfigStore:
    """Add all models from hydra_models to the config store.

    Groups that have elements which can be instantiated at the same time are included using subgroups. For example
    can one use accuracy and r2_score at the same time. Therefore, they are included in the group metrics with a
    subgroup for each of them.

    Args:
        config_store (ConfigStore): The config store to add the models to.
        rename_groups (dict): A dictionary containing the groups to rename as keys and the new names as values.

    Examples:

        .. code-block:: python

            from hydra.core.config_store import ConfigStore
            from mdml_tools.utils import add_hydra_models_to_config_store
            cs = ConfigStore.instance()
            cs = add_hydra_models_to_config_store(cs)

        Each config group can be renamed:

        .. code-block:: python

            from hydra.core.config_store import ConfigStore
            from mdml_tools.utils import add_hydra_models_to_config_store
            cs = ConfigStore.instance()
            cs = add_hydra_models_to_config_store(cs, rename_groups={"models": "model/head"})

        If multiple values are given for a group key, the group is registered multiple times using all values:

        .. code-block:: python

            from hydra.core.config_store import ConfigStore
            from mdml_tools.utils import add_hydra_models_to_config_store
            cs = ConfigStore.instance()
            rename_groups = {"optimizer": ["generator_optimizer", "discriminator_optimizer"]}
            cs = add_hydra_models_to_config_store(cs, rename_groups=rename_groups)

        So later in your config file you can use:

        .. code-block:: yaml
            :caption: config.yaml

            defaults:
              - generator_optimizer: adam_base
              - discriminator_optimizer: sgd_base
              - _self_

            ...
    """

    # renaming group keys:
    if rename_groups is not None:
        for old_key, new_key in rename_groups.items():
            CONFIG_GROUPS[old_key] = new_key

    # packing keys which are str into lists
    for key, value in CONFIG_GROUPS.items():
        if not isinstance(value, list):
            CONFIG_GROUPS[key] = [value]

    # adding models to config store:
    for key, named_group in CONFIG_GROUPS.items():
        for group_name in named_group:
            if key in CONFIGS_THAT_ARE_LISTS:
                config_store = _add_module_to_config_store_group(
                    config_store, CONFIG_GROUPS_MODULES_MAPPING[key], group_name, True
                )
            else:
                config_store = _add_module_to_config_store_group(
                    config_store, CONFIG_GROUPS_MODULES_MAPPING[key], group_name, False
                )

    return config_store
