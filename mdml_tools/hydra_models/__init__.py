"""Hydra models for pytorch and pytorch-lightning.

See :py:mod:`mdml_tools.hydra_models.typing` for an example of how to use this module and the type hints it provides.

Examples:
    We use the optimizer as an example. The following code will instantiate the optimizer and give you type hints for
    the optimizer instance:

    .. code-block:: python

        import hydra
        from omegaconf import DictConfig
        from mdml_tools.hydra_models import Optimizer

        @hydra.main(config_path="conf", config_name="config")
        def main(cfg: DictConfig) -> None:
            my_optimizer: Optimizer = hydra.utils.instantiate(cfg.optimizer)

        # my_optimizer is now an instance of torch.optim.Optimizer and can be used as such, including type hints.


    If you know the type of the optimizer, you can also use the following to get more specific type hints:

    .. code-block:: python

        import hydra
        from omegaconf import DictConfig
        from mdml_tools.hydra_models.optimizer import Adam

        @hydra.main(config_path="conf", config_name="config")
        def main(cfg: DictConfig) -> None:
            my_optimizer: Adam = hydra.utils.instantiate(cfg.optimizer)

            # my_optimizer is now an instance of torch.optim.Adam, which allows for more specific type hints like:
            rho = my_optimizer.rho  # this would be marked as non-existent in the previous example, because not all
                                    # optimizers have a rho parameter.
"""

from mdml_tools.hydra_models.dataloading import DataLoader
from mdml_tools.hydra_models.lightning_trainer import Trainer
from mdml_tools.hydra_models.typing import (
    DataScaler,
    LightningCallback,
    LightningLogger,
    LightningProfiler,
    LightningStrategy,
    Loss,
    Metric,
    Model,
    Optimizer,
    Scheduler,
)

__all__ = [
    "Trainer",
    "LightningProfiler",
    "Optimizer",
    "Scheduler",
    "LightningCallback",
    "LightningLogger",
    "Loss",
    "Model",
    "Metric",
    "LightningStrategy",
    "DataLoader",
    "DataScaler",
]
