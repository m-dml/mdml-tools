"""Module containing structured configs for data preprocessing."""

from dataclasses import dataclass
from typing import Any, Union


@dataclass
class _BaseScaler:
    """Base Hydra dataclass for :py:class:`mdml_tools.utils.preprocessing._BaseScaler`"""

    _target_: str = "mdml_tools.utils.preprocessing._BaseScaler"
    data_loader_samples: Union[float, int, None] = None
    data_loader: Any = None
    data_loader_dim: int = 0


@dataclass
class MinMaxScaler(_BaseScaler):
    """Hydra dataclass for :py:class:`mdml_tools.utils.preprocessing.MinMaxScaler`"""

    _target_: str = "mdml_tools.utils.preprocessing.MinMaxScaler"
    data_min: Any = None
    data_max: Any = None
    target_min: float = 0
    target_max: float = 1
    clip_min: Any = None
    clip_max: Any = None


@dataclass
class NormalScaler(_BaseScaler):
    """Hydra dataclass for :py:class:`mdml_tools.utils.preprocessing.NormalScaler`"""

    _target_: str = "mdml_tools.utils.preprocessing.NormalScaler"
    data_mean: Any = None
    data_std: Any = None
    target_mean: float = 0
    target_std: float = 1
