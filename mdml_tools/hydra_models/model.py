"""Hydra dataclasses for models in :py:mod:`torchvision.models` and :py:mod:`mdml_tools.model_zoo`."""

from dataclasses import dataclass
from typing import Any

from omegaconf import MISSING


@dataclass
class ResNet:
    """Hydra dataclass for :py:class:`torchvision.models.resnet.ResNet`

    Example:
        By creating a yaml-file one can change the underlying model architecture:

        .. code-block:: yaml
            :caption: resnet52.yaml

            defaults:
                - resnet

            _target_: torchvision.models.resnet52
    """

    _target_: str = "torchvision.models.resnet18"
    num_classes: int = 1000  # has to be 1000 for pretrained model
    weights: Any = None


@dataclass
class CustomResnet:
    """See :class:`mdml_tools.model_zoo.resnet.CustomResnet` for more details."""

    _target_: str = "mdml_tools.model_zoo.resnet.CustomResnet"
    kernel_size: int = 7
    stride: int = 2
    channels: int = 3
    model: Any = MISSING
    maxpool1: bool = True


@dataclass
class FullyConnectedModel:
    """See :class:`mdml_tools.model_zoo.fully_connected.FullyConnectedModel` for more details."""

    _target_: str = "mdml_tools.model_zoo.fully_connected.FullyConnectedModel"
    hidden_layers: Any = (1000, 1000)
    activation: Any = MISSING
    input_features: int = 1000
    num_classes: Any = MISSING
    normalize: bool = False
    bias_in_last_layer: bool = True
    use_batch_norm: bool = False
