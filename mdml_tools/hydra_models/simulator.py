from dataclasses import dataclass
from typing import Any, Union


@dataclass
class _BaseSimulator:
    """Hydra dataclass for :py:class:`mdml_tools.simulators.base.BaseSimulator`."""

    # hydra args:
    _target_: str = "mdml_tools.simulators.base.BaseSimulator"
    _recursive_: bool = True
    _convert_: Any = None
    _partial_: bool = False

    # dataclass args:
    use_adjoint: bool = True
    method: str = "rk4"
    options: Union[dict, None] = None


@dataclass
class L96SimulatorOneLevel(_BaseSimulator):
    """Hydra dataclass for :py:class:`mdml_tools.simulators.lorenz96.L96SimulatorOneLevel`."""

    # hydra args:
    _target_: str = "mdml_tools.simulators.lorenz96.L96SimulatorOneLevel"

    # dataclass args:
    forcing: float = 8.0


@dataclass
class L96SimulatorTwoLevel(_BaseSimulator):
    """Hydra dataclass for :py:class:`mdml_tools.simulators.lorenz96.L96SimulatorTwoLevel`."""

    # hydra args:
    _target_: str = "mdml_tools.simulators.lorenz96.L96SimulatorTwoLevel"

    # dataclass args:
    forcing: float = 8.0
    b: float = 10.0
    c: float = 1.0
    h: float = 10.0


@dataclass
class L96Simulator(L96SimulatorOneLevel, L96SimulatorTwoLevel):
    """Hydra dataclass for :py:class:`mdml_tools.simulators.L96Simulator`."""

    # hydra args:
    _target_: str = "mdml_tools.simulators.L96Simulator"

    # dataclass args:
    simulator_type: str = "one_level"  # "one_level" or "two_level"
