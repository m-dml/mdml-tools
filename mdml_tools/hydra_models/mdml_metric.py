"""Hydra models for custom metrics."""

from dataclasses import dataclass
from typing import Any


@dataclass
class RootEuclidianDistance:
    """Calculates the root euclidian distance between the ensemble inputs and the targets.

    The metric is from the paper "Pareto GAN: Extending the Representational Power of GANs to Heavy-Tailed
    Distributions" eq. 12. It calculates the L2 norm and takes the nth root of it.

    Args:
        root (int): Which root to take of the L2 norm. Default is 2 (=square root).
    """

    _target_: str = "mdml_tools.utils.metrics.RootEuclidianDistance"
    _recursive_: bool = True
    _convert_: Any = None
    _partial_: bool = False

    root: int = 2
