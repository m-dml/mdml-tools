"""This module contains typing classes for Hydra models.

They are intended to be used in your python scripts where you instantiate the instance of a certain configuration.

Examples:
    We use the optimizer as an example. The following code will instantiate the optimizer and give you type hints for
    the optimizer instance:

    .. code-block:: python

        import hydra
        from omegaconf import DictConfig
        from mdml_tools.hydra_models import Optimizer

        @hydra.main(config_path="conf", config_name="config")
        def main(cfg: DictConfig) -> None:
            my_optimizer: Optimizer = hydra.utils.instantiate(cfg.optimizer)

        # my_optimizer is now an instance of torch.optim.Optimizer and can be used as such, including type hints.


    If you know the type of the optimizer, you can also use the following to get more specific type hints:

    .. code-block:: python

        import hydra
        from omegaconf import DictConfig
        from mdml_tools.hydra_models.optimizer import Adam

        @hydra.main(config_path="conf", config_name="config")
        def main(cfg: DictConfig) -> None:
            my_optimizer: Adam = hydra.utils.instantiate(cfg.optimizer)

            # my_optimizer is now an instance of torch.optim.Adam, which allows for more specific type hints like:
            rho = my_optimizer.rho  # this would be marked as non-existent in the previous example, because not all
                                    # optimizers have a rho parameter.
"""

from abc import ABC
from dataclasses import dataclass
from typing import Any, Union

from pytorch_lightning.callbacks import Callback
from pytorch_lightning.loggers.logger import Logger
from pytorch_lightning.profilers import Profiler
from pytorch_lightning.strategies import Strategy
from torch.nn import Module
from torch.nn.modules.loss import _Loss
from torch.optim import Optimizer as TorchOptimizer
from torch.optim.lr_scheduler import _LRScheduler
from torchmetrics import Metric as BaseMetric

from .dataloading import DataLoader
from .distribution import Distribution
from .lightning_trainer import Trainer
from .preprocessing import _BaseScaler

__all__ = [
    "Trainer",
    "Scheduler",
    "Optimizer",
    "Model",
    "Loss",
    "Metric",
    "DataLoader",
    "LightningProfiler",
    "LightningStrategy",
    "LightningCallback",
    "LightningLogger",
    "Distribution",
    "DataScaler",
]


@dataclass
class Scheduler(_LRScheduler, ABC):
    """Typing class for Schedulers in :py:mod:`mdml_tools.hydra_models.scheduler` ."""

    _target_: str
    _recursive_: bool = False
    optimizer: Any = None


@dataclass
class Optimizer(TorchOptimizer, ABC):
    """Typing class for Optimizers in :py:mod:`mdml_tools.hydra_models.optimizer` .

    Examples:

        .. code-block:: python

            import hydra
            from omegaconf import DictConfig
            from mdml_tools.hydra_models import Optimizer

            @hydra.main(config_path="conf", config_name="config")
            def main(cfg: DictConfig) -> None:
                my_optimizer: Optimizer = hydra.utils.instantiate(cfg.optimizer)

            # my_optimizer is now an instance of torch.optim.Optimizer and can be used as such, including type hints.


        If you know the type of the optimizer, you can also use the following to get more specific type hints:

        .. code-block:: python

            import hydra
            from omegaconf import DictConfig
            from mdml_tools.hydra_models.optimizer import Adam

            @hydra.main(config_path="conf", config_name="config")
            def main(cfg: DictConfig) -> None:
                my_optimizer: Adam = hydra.utils.instantiate(cfg.optimizer)

                # my_optimizer is now an instance of torch.optim.Adam, which allows for more specific type hints like:
                rho = my_optimizer.rho  # this would be marked as non-existent in the previous example, because not all
                                        # optimizers have a rho parameter.
    """

    _target_: str
    lr: float


@dataclass
class Model(Module, ABC):
    """Typing class for models in :py:mod:`mdml_tools.hydra_models.models` .

    Example:

        .. code-block:: python

            import hydra
            from omegaconf import DictConfig
            from mdml_tools.hydra_models import Model

            @hydra.main(config_path="conf", config_name="config")
            def main(cfg: DictConfig) -> None:
                my_model: Model = hydra.utils.instantiate(cfg.model)

            # my_model is now an instance of torch.nn.Module and can be used as such, including type hints.
    """

    _target_: str


@dataclass
class Metric(BaseMetric, ABC):
    """Typing class for Metrics in :py:mod:`mdml_tools.hydra_models.metrics` .

    Example:

        .. code-block:: python

            import hydra
            from omegaconf import DictConfig
            from mdml_tools.hydra_models import Metric

            @hydra.main(config_path="conf", config_name="config")
            def main(cfg: DictConfig) -> None:
                my_metric: Metric = hydra.utils.instantiate(cfg.metric)

            # my_metric is now an instance of torchmetrics.Metric and can be used as such, including type hints.
    """

    _target_: str


@dataclass
class Loss(_Loss, ABC):
    """Typing class for Losses in :py:mod:`mdml_tools.hydra_models.loss` .

    Example:

        .. code-block:: python

            import hydra
            from omegaconf import DictConfig
            from mdml_tools.hydra_models import Loss

            @hydra.main(config_path="conf", config_name="config")
            def main(cfg: DictConfig) -> None:
                my_loss: Loss = hydra.utils.instantiate(cfg.loss)

            # my_loss is now an instance of torch.nn.modules.loss._Loss and can be used as such, including type hints.
    """

    _target_: str
    _size_average: Union[bool, None]
    reduce: Union[bool, None]
    reduction: str


@dataclass
class LightningStrategy(Strategy, ABC):
    """Typing class for Lightning strategies in :py:mod:`mdml_tools.hydra_models.lightning_strategies`"""

    _target_: str


@dataclass
class LightningProfiler(Profiler, ABC):
    """Typing class for Lightning profilers in :py:mod:`mdml_tools.hydra_models.lightning_profiler` ."""

    _target_: str


@dataclass
class LightningCallback(Callback, ABC):
    """Typing class for Lightning callbacks in :py:mod:`mdml_tools.hydra_models.lightning_callbacks` ."""

    _target_: str


@dataclass
class LightningLogger(Logger, ABC):
    """Typing class for Lightning callbacks in :py:mod:`mdml_tools.hydra_models.lightning_loggers` ."""

    _target_: str


class DataScaler(_BaseScaler, ABC):
    """Typing class for preprocessing scalers in :py:mod:`mdml_tools.hydra_models.preprocessing` ."""

    _target_: str
