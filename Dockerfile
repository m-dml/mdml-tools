#syntax=docker/dockerfile:1.3-labs

FROM mambaorg/micromamba:1.3-jammy-cuda-11.7.1

ARG BUILDKIT_CONTEXT_KEEP_GIT_DIR=1
ARG ENVIRONMENT_FILE=dev_environment2.yaml
ARG PROJECT_NAME=mdml_tools
ARG CONDA_CACHE_DIR="./conda_cache"

RUN micromamba shell init -s bash -p /opt/conda/etc/profile.d/mamba.sh

# create condarc file to enable shared cache
RUN <<EOF
echo "pkgs_dirs:" >> ~/.mambarc
echo "  - /opt/conda/pkgs" >> ~/.mambarc
echo "  - /opt/conda/etc/profile.d/mamba.sh/pkgs" >> ~/.mambarc
EOF

COPY --chown=$MAMBA_USER:$MAMBA_USER $ENVIRONMENT_FILE /tmp/env.yaml

ARG MAMBA_DOCKERFILE_ACTIVATE=1
# Install dependencies but use the cache if possible
RUN --mount=type=cache,mode=0777,target=/opt/conda/pkgs/ \
    --mount=type=cache,mode=0777,target=/opt/conda/etc/profile.d/mamba.sh/pkgs/ \
    micromamba install -y -n base -f /tmp/env.yaml

COPY --chown=$MAMBA_USER:$MAMBA_USER . /tmp/$PROJECT_NAME
RUN pip install -e /tmp/$PROJECT_NAME
